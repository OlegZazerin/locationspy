import React, {Component} from 'react';
import {
  Dimensions,
  StyleSheet,
  View,
  Text,
  Image,
  TouchableNativeFeedback
} from 'react-native';

import GoogleApi from '../api/GoogleApi';
const GApi = new GoogleApi();

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Actions, ActionConst } from 'react-native-router-flux';

import _ from 'lodash';

import * as appActions from '../actions/appActions';

import MapView from 'react-native-maps';
import Marker from '../components/Marker';

const backIconLightSrc = require('../components/img/back_light.png');
const defaultMarkerDraggable = require('../components/img/default_marker_draggable.png');
const sm = require('../components/img/select_marker.png');

import InfoBar from '../components/SelectOnMap/InfoBar';
import BackToOriginalLocationBtn from '../components/BackToOriginalLocationBtn';

import * as utils from '../utils/utils';

class SelectOnMap extends Component {
    constructor(props) {
      super(props);

      const { width, height } = Dimensions.get('window');

      this.state = {
        initialRegion: null,
        region: null,
        originalCoordinate: null,
        descriptionPlace: null,
        LATITUDE_DELTA: 0.0041,
        LONGITUDE_DELTA: 0.0041 * width/height,
        styleMarkerContainer: {
          position: 'absolute',
          top: height/2,
          left: width/2
        }
      };
    }
    componentWillMount() {
      const { mockLocation, originalLocation } = this.props.location;
      let region = null, originalCoordinate = null;

      if(mockLocation.coordinate) {
        region = {
          latitude: mockLocation.coordinate.latitude,
          longitude: mockLocation.coordinate.longitude,
          latitudeDelta: this.state.LATITUDE_DELTA,
          longitudeDelta: this.state.LONGITUDE_DELTA
        };

        this.getDescriptionPlase(region);
      }

      if(originalLocation) {
        originalCoordinate = {
          latitude: originalLocation.latitude,
          longitude: originalLocation.longitude
        };
      }

      this.setState({ region, originalCoordinate });
    }
    onRegionChange = region => {
      this.getDescriptionPlase(region);
      this.setState({region});
    }
    getDescriptionPlase = _.debounce(region => {
      GApi.geocodeLocation(region)
      .then(res => {
        if(res.status == "OK") {
          this.setState({
            descriptionPlace: {
              address: utils.getFormatedAddress(res.results[0].address_components),
              locality: utils.getFormatedLocality(res.results[0].address_components)
            }
          });
        }
      })
      .catch(error => {
        console.log(error);
      });
    }, 500) 
    backToCurrentPosition = () => {
      const { originalCoordinate } = this.state;
      if(originalCoordinate) {
        this.refs.map.animateToRegion({
          ...originalCoordinate,
          latitudeDelta: this.state.LATITUDE_DELTA,
          longitudeDelta: this.state.LONGITUDE_DELTA
        });
      }
    }
    render() {
      const { initialRegion, region, originalCoordinate, descriptionPlace }  = this.state;
      return (
          <View style={styles.container}>
            <View style={styles.barConteiner}>
              <TouchableNativeFeedback onPress={Actions.pop}>
                <Image source={backIconLightSrc} style={styles.backIcon}/>
              </TouchableNativeFeedback>
              <Text style={styles.headerTitle}>Select preferable location</Text>
            </View>
            <MapView
              ref='map'
              style={styles.map}
              initialRegion={initialRegion}
              region={region}
              onRegionChange={this.onRegionChange}>
                { this.renderMapContent() }
            </MapView>
            <View style={[this.state.styleMarkerContainer]}>
              { 
                this.isShowOriginalPosition() || !originalCoordinate ? 
                <Image source={sm} style={styles.newLocationMarker}/> 
                : 
                <Image source={defaultMarkerDraggable} style={styles.actualLocationMarker}/>
              }
            </View> 
            <InfoBar 
              coordinate={region || originalCoordinate}
              description={descriptionPlace}
              onPressOk={value => {
                this.props.setMockLocation(value);
                Actions.popTo("main");
              }} 
              onPressCancel={Actions.pop} />
            <BackToOriginalLocationBtn
              show={originalCoordinate}
              onPress={this.backToCurrentPosition} />
          </View>
      );
    }
    renderMapContent = () => {
      if(this.state.originalCoordinate === null) return null;

      if(this.isShowOriginalPosition()) {
        return <Marker 
                type='default' 
                draggable={false} 
                coordinate={this.state.originalCoordinate} />
      } else {
        return <MapView.Circle
                fillColor={'rgba(47, 60, 79, 0.1)'}
                radius={40}
                strokeWidth={0}
                center={this.state.originalCoordinate} />
      }
    }
    isShowOriginalPosition = () => {
      const { region, originalCoordinate }  = this.state;
      if(!region || !originalCoordinate) return false;
      const diff = utils.getDistanceFromLatLonInMeters(
        region.latitude, 
        region.longitude, 
        originalCoordinate.latitude, 
        originalCoordinate.longitude
      );
      return diff >= 5; 
    }
}

const mapDispatchToProps = dispatch => bindActionCreators(appActions, dispatch);

const mapStateToProps = ({ location }) => ({ location });

export default connect(mapStateToProps, mapDispatchToProps)(SelectOnMap);

const styles = StyleSheet.create({
    container: {
      flex: 1,
      position: 'relative',
      justifyContent: 'space-between',
    },
    barConteiner: {
      backgroundColor: 'rgba(47, 60, 79, 0.95)',
      height: 76,
      alignItems: 'center',
      flexDirection: 'row',
      justifyContent: 'flex-start',
      zIndex: 1
    },
    headerTitle: {
      fontFamily: 'AvenirNextLTPro-Bold',
      fontSize: 18,
      color: '#fff',
      lineHeight: 20
    },
    backIcon: {
      width: 22,
      height: 22,
      marginHorizontal: 22
    },
    map: {
        ...StyleSheet.absoluteFillObject
    },
    actualLocationMarker: {
      position: 'relative',
      top: -32,
      left: -20,
      width: 40,
      height: 40,
    },
    newLocationMarker: {
      position: 'relative',
      top: -52,
      left: -15,
      width: 25,
      height: 55,
    }
});
