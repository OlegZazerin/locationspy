
import React, { Component } from 'react';
import { StatusBar, View, StyleSheet, Image, Dimensions, ToastAndroid } from 'react-native';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

const fixedMarkerImg = require('../components/img/fixed_marker.png');
const defaultMarkerDraggable = require('../components/img/default_marker_draggable.png');
const movedMarker = require('../components/img/moved_marker.png');

import * as appActions from '../actions/appActions';

import MapView from 'react-native-maps';
import MockLocationManger from '../api/MockLocationManager';
import { Actions } from 'react-native-router-flux';
import _ from 'lodash';
import * as utils from '../utils/utils';

import SearchBar from '../components/Location/SearchBar';
import Marker from '../components/Marker';
import LocationManager from '../components/Location/Manager';

import BackToOriginalLocationBtn from '../components/BackToOriginalLocationBtn';

export class Location extends Component {
    constructor(props) {
        super(props);

        const { width, height } = Dimensions.get('window');

        this.state = {
            region: null,
            originalRegion: null,
            originalCoordinate: null,
            descriptionPlace: null,
            LATITUDE_DELTA: 0.0041,
            LONGITUDE_DELTA: 0.0041 * width/height,
            styleMarkerContainer: {
                position: 'absolute',
                top: height/2,
                left: width/2,
                zIndex: 1000
            },
           isAddedProvider: false
        }
    }
    componentWillMount() {
        const { mockLocation, originalLocation } = this.props.location;
        let region = null, originalRegion = null, originalCoordinate = null;
        
        if(mockLocation.coordinate) {
            region = {
                latitude: mockLocation.coordinate.latitude,
                longitude: mockLocation.coordinate.longitude,
                latitudeDelta: this.state.LATITUDE_DELTA,
                longitudeDelta: this.state.LONGITUDE_DELTA
            };
        }

        if(originalLocation) {
            originalCoordinate = {
                latitude: originalLocation.latitude,
                longitude: originalLocation.longitude
            };
            originalRegion = {
                latitude: originalLocation.latitude,
                longitude: originalLocation.longitude,
                latitudeDelta: this.state.LATITUDE_DELTA,
                longitudeDelta: this.state.LONGITUDE_DELTA
            }
        }

        this.setState({ region, originalRegion, originalCoordinate });
    }
    componentWillReceiveProps(nextProps) {
        const { mockLocation } = nextProps.location;
        const prevMockLocation = this.props.location.mockLocation;
        const { region } = this.state;

        if( mockLocation.coordinate != prevMockLocation.coordinate 
            && (region.latitude != mockLocation.coordinate.latitude || region.longitude != mockLocation.coordinate.longitude)
            ) {
            this.updateRegion(mockLocation.coordinate);
        }
    
    }
    shouldComponentUpdate(nextProps, nextState) {
        return !_.isEqual(nextProps, this.props) || !_.isEqual(nextState, this.state);
    }
    updateRegion = coordinate => {
        const region = {
            latitude: coordinate.latitude,
            longitude: coordinate.longitude,
            latitudeDelta: this.state.LATITUDE_DELTA,
            longitudeDelta: this.state.LONGITUDE_DELTA
        };

        this.setState({ region });
    }    
    onRegionChange = region => {
        this.onMockLocationChange(region);
        this.setState({region});
    }
    onMockLocationChange = _.debounce(region => {
        this.props.setMockLocation(region);
    }, 500)
    backToCurrentPosition = () => {
        const { originalRegion } = this.state;

        if(originalRegion == null) return;

        this.refs.map.animateToRegion(originalRegion);
    }
    render() {
        const { access, map, location } = this.props;
        const { originalCoordinate, region } = this.state;
        console.log('render location');
        return (
            <View style={styles.container}>
                <SearchBar hold={location.isActiveMockLocation} onPress={() => { Actions.search() }}/>
                <MapView
                    style={styles.map}
                    ref='map'
                    region={region}
                    onRegionChangeComplete={this.onRegionChangeComplete}
                    onRegionChange={this.onRegionChange}>
                    { this.renderMapContent() }
                </MapView>
                { location.isActiveMockLocation ? <View style={styles.overlay} /> : null }
                { this.renderMarker() }
                <LocationManager
                    show={access.isAllowedUseApp}
                    isActiveMockLocation={location.isActiveMockLocation}
                    originalLocation={location.originalLocation}
                    mockLocation={location.mockLocation}
                    onPress={this.toggleMockLocation}/>
                <BackToOriginalLocationBtn
                    show={access.isAllowedUseApp && !location.isActiveMockLocation && originalCoordinate}
                    onPress={this.backToCurrentPosition} />
            </View>
        );
    }
    renderMarker = () => {
        if(this.props.location.isActiveMockLocation) {
            return <Image source={fixedMarkerImg} style={[this.state.styleMarkerContainer, styles.fixedMarker]}/> 
        } else if(this.isShowOriginalPosition() || !this.state.originalCoordinate) {
            return <Image source={movedMarker} style={[this.state.styleMarkerContainer,styles.newLocationMarker]}/> 
        } else if(this.state.originalCoordinate) {
            return <Image source={defaultMarkerDraggable} style={[this.state.styleMarkerContainer,styles.actualLocationMarker]}/>
        } else {
            return null;
        }         
    }
    renderMapContent = () => {
        if(this.state.originalCoordinate === null) return null;

        if(this.isShowOriginalPosition()) {
            return <Marker 
                    type='default' 
                    draggable={false} 
                    coordinate={this.state.originalCoordinate} />
        } else {
            return <MapView.Circle
                    fillColor={'rgba(47, 60, 79, 0.1)'}
                    radius={40}
                    strokeWidth={0}
                    center={this.state.originalCoordinate} />
        }
    }
    isShowOriginalPosition = () => {
        const { region, originalCoordinate }  = this.state;
        if(!region || !originalCoordinate) return false;
        const diff = utils.getDistanceFromLatLonInMeters(
            region.latitude, 
            region.longitude, 
            originalCoordinate.latitude, 
            originalCoordinate.longitude
        );
        return diff >= 5; 
    }
    toggleMockLocation = () => {
        const { isActiveMockLocation, mockLocation, originalLocation } = this.props.location;
        const { isAddedProvider } = this.state;

        if(isActiveMockLocation) {
            if(originalLocation) {
                MockLocationManger
                .addNewLocation(originalLocation.latitude, originalLocation.longitude);
            }
            MockLocationManger.clearLocation();
            this.props.changeStatusMockLocation(false);
        } else if(mockLocation.coordinate){
            MockLocationManger.addGPSProvider()
            .then(() => {
                MockLocationManger
                .addNewLocation(mockLocation.coordinate.latitude, mockLocation.coordinate.longitude)
                .then(() => {
                    this.props.changeStatusMockLocation(true);
                })
                .catch(() => {
                    console.log('failed to add the coordinates');
                });    
            }).catch(() => {
                console.log('provider is not added');
            });
        }
    }      
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        position: 'relative',
        justifyContent: 'space-between',
    },
    map: {
        ...StyleSheet.absoluteFillObject
    },
    actualLocationMarker: {
        marginTop: -32,
        marginLeft: -20,
        width: 40,
        height: 40,
    },
    newLocationMarker: {
        marginTop: -62,
        marginLeft: -12,
        width: 25,
        height: 55,
    },
    fixedMarker: {
        zIndex: 100,
        marginTop: -52,
        marginLeft: -30,
        width: 42,
        height: 41, 
    },
    overlay: {
        ...StyleSheet.absoluteFillObject,
        backgroundColor: 'rgba(0,0,0,0.3)'
    }
});

function mapDispatchToProps(dispatch) {
    return bindActionCreators(appActions, dispatch);
}

function mapStateToProps(state) {
    const { access, location } = state;
    return {
        access,
        location
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Location);
