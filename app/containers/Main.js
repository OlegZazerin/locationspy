import React, { Component } from 'react';
import { View, StyleSheet, Dimensions } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { TabViewAnimated, TabViewPagerPan, TabBarTop } from 'react-native-tab-view';
import TabBar from '../components/UI/TabBar';

import * as appActions from '../actions/appActions';
import * as recentActions from '../actions/recentActions';

import Location from './Location';
import Direction from './Direction';

class Main extends Component {
    state = {
        index: 0,
        routes: [
            { key: '1', title: 'Location' },
            { key: '2', title: 'Direction' },
        ],
    };

    componentWillMount () {
        this.props.getLastKnownLocation();
        this.props.loadingRecentData();     
    }
    
    _handleChangeTab = index => {
      this.setState({ index });
    };

    _renderHeader = props => {
        return <TabBar hold={this.props.hold} scrollEnabled={false} {...props} />;
    };

    _renderScene = ({ route }) => {
        switch (route.key) {
            case '1':
                return <Location />;
            case '2':
                return <Direction />;
            default:
                return null;
        }
    };

    _renderPager = props => {
        return <TabViewPagerPan {...props} swipeEnabled={false} />;
    };

    render() {
        return (
            <TabViewAnimated
                style={{flex: 1}}
                navigationState={this.state}
                renderScene={this._renderScene}
                renderHeader={this._renderHeader}
                renderPager={this._renderPager}
                onRequestChangeTab={this._handleChangeTab}
            />
        );
    }
}

const mapDispatchToProps = dispatch => bindActionCreators({...appActions, ...recentActions}, dispatch);

const mapStateToProps = state => ({ hold: state.location.isActiveMockLocation || state.direction.isActiveMockLocation });

export default connect(mapStateToProps, mapDispatchToProps)(Main);
