import React, { Component } from 'react';
import { AppState, View } from 'react-native';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { DefaultRenderer } from 'react-native-router-flux';

import MLManager from '../api/MockLocationManager';

import * as appActions from '../actions/appActions';

import ActionsRequiredModal from '../components/ActionsRequiredModal';

export class AccessManager extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isAllowedUseApp: undefined,
            isDebuggable: undefined,
            isAllowedMockLocations: undefined
        };
    }
    componentWillMount() {
        MLManager.getAllRequiredSettings((isDebuggable, isAllowedMockLocations) => {
            let isAllowedUseApp = false;
            if(isDebuggable && isAllowedMockLocations){
                isAllowedUseApp = true;
            }
            this.props.changeFlagAllowedUseApp(isAllowedUseApp);
            this.setState({isDebuggable, isAllowedMockLocations, isAllowedUseApp});
        });

        AppState.addEventListener('change', this._handleAppStateChange.bind(this));
    }
    componentWillUnmount() {
        AppState.removeEventListener('change', this._handleAppStateChange.bind(this));
    }
    _handleAppStateChange(currentAppState){
        if(currentAppState === 'active') {
            MLManager.getAllRequiredSettings((isDebuggable, isAllowedMockLocations) => {
                let { isAllowedUseApp } = this.state;
                if(!isDebuggable || !isAllowedMockLocations){
                    isAllowedUseApp = false;
                }
                this.setState({isDebuggable, isAllowedMockLocations, isAllowedUseApp});
                this.props.changeFlagAllowedUseApp(isAllowedUseApp);
            });
        }
    }
    render() {
        const { isDebuggable, isAllowedMockLocations, isAllowedUseApp } = this.state;
        const state = this.props.navigationState;
        const children = state.children;

        if(isAllowedUseApp === undefined) {
            return null;
        } else if(!isAllowedUseApp) {
            return (
                <View style={{flex: 1}}>
                    <DefaultRenderer navigationState={children[0]} onNavigate={this.props.onNavigate} />
                    <ActionsRequiredModal
                        isDebuggable={isDebuggable}
                        isAllowedMockLocations={isAllowedMockLocations}
                        onPress={() => {
							this.setState({ isAllowedUseApp: true });
							this.props.changeFlagAllowedUseApp(true)
						}}/>
                </View>
            );
        } else {
            return <DefaultRenderer navigationState={children[0]} onNavigate={this.props.onNavigate} />
        }
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(appActions, dispatch);
}

function mapStateToProps(state) {
    return state.access;
}

export default connect(mapStateToProps, mapDispatchToProps)(AccessManager);
