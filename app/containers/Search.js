import React, {Component} from 'react';
import {
  StyleSheet,
  ScrollView,
  View,
  Text,
  TextInput,
  Image,
  TouchableNativeFeedback,
  ToastAndroid
} from 'react-native';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Actions, ActionConst } from 'react-native-router-flux';

const actualLocationIcon = require('../components/img/actual_location_icon.png');
const onMapIcon = require('../components/img/on_map_icon.png');

import Button from '../components/Search/Button';
import RecentSearchesList from '../components/RecentSearchesList';
import GooglePlacesAutocomplete from '../components/GooglePlacesAutocomplete';
import GoogleAutocompleteItem from '../components/GoogleAutocompleteItem';

const backIconDarkSrc = require('../components/img/back_dark.png');

import * as appActions from '../actions/appActions';
import * as recentActions from '../actions/recentActions';

class Search extends Component {
    constructor(props) {
      super(props);

      this.state = {
        queryParameters: {
           key: 'AIzaSyD7f1KjELWTkxKf0z44euJVMZjKkSuANGM',
           language: 'en',
           types: 'address'
        },
        inputStyles: {
          textInputContainer: styles.inputContainer,
          textInput: styles.input,
          poweredContainer: {height: 0},
          powered: {height: 0},
          listView: styles.listView
        }
      }
    }
    render() {
      const { actualLocation, recent } = this.props;
      const { queryParameters, inputStyles } = this.state;

      return (
          <View style={styles.container}>
            <View style={styles.searchContainer}>
              <View style={styles.inputWrapper}>
                <View style={styles.backIconContainer}>
                  <TouchableNativeFeedback onPress={Actions.pop}>
                    <Image source={backIconDarkSrc} style={styles.backIcon}/>
                  </TouchableNativeFeedback>
                </View>
                <GooglePlacesAutocomplete
                  placeholder='Enter the address'
                  minLength={2}
                  autoFocus={false}
                  fetchDetails={true}
                  textInputProps={{ onFocus: () => {} }}
                  onPress={data => {
                    this.onSelectLocation(data);
                    this.props.addSearches(data);
                  }}
                  getDefaultValue={() => {}}
                  query={queryParameters}
                  styles={inputStyles}
                  recentResults={recent.searches}
                  currentLocation={false} />
              </View>
            </View>
            <View style={styles.searchBarBackground} />
            <View style={styles.buttonsContainer}>
              <Button
                text="Select on map"
                source={onMapIcon}
                styleIcon={{width: 14, height: 22}}
                onPress={Actions.selectOnMap}/>
              <Button
                text="Actual location"
                source={actualLocationIcon}
                hide={!actualLocation}
                styleIcon={{width: 15, height: 15}}
                onPress={() => {
                    this.props.setMockLocation(actualLocation);
                    Actions.popTo("main");
                }}/>
            </View>
            <View style={{flex: 1, zIndex: 1}}>
              <RecentSearchesList
                data={recent.searches}
                onSelect={this.onSelectLocation} />
            </View>    
          </View> 
      );
    }
    onSelectLocation = item => {
      const coordinate = {
        latitude: item.geometry.location.lat,
        longitude: item.geometry.location.lng
      }
      this.props.setMockLocation(coordinate);
      Actions.popTo("main");
    }
}

const mapDispatchToProps = dispatch => bindActionCreators({...appActions, ...recentActions}, dispatch);

const mapStateToProps = ({ location, recent }) => ({ actualLocation: location.originalLocation, recent });

export default connect(mapStateToProps, mapDispatchToProps)(Search);


const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: 'rgb(250, 249, 247)'
    },
    buttonsContainer: {
      flexDirection: 'row',
      alignItems: 'stretch',
      marginHorizontal: 24,
      marginVertical: 35
    },
    backIconContainer: {
      position: 'absolute',
      height: 26,
      justifyContent: 'center',
      alignItems: 'center',
      borderRightWidth: 1.6,
      borderRightColor: '#394658',
      paddingHorizontal: 22,
      marginVertical: 8,
      zIndex: 1
    },
    backIcon: {
    },
    searchContainer: {
      position: 'absolute',
      top: 0,
      left: 0,
      right: 0,
      zIndex: 10,
      justifyContent: 'center',
      paddingHorizontal: 20,
      paddingBottom: 10
    },
    searchBarBackground: {
      height: 84,
      backgroundColor: 'rgba(47, 60, 79, 0.95)',
    },
    map: {
      ...StyleSheet.absoluteFillObject
    },
    listView: {
      marginTop: 50,
      borderRadius: 4,
      backgroundColor: '#fff',
      elevation : 3,
      marginBottom: 300,
      zIndex: 100
    },
    input: {
      flex: 1,
      marginLeft: 60,
      alignSelf: 'stretch',
      fontFamily: 'AvenirNextLTPro-Demi',
      fontSize: 15,
      color: 'rgba(53, 67, 88, 0.702)'
    },
    inputContainer: {
      borderRadius: 4,
      backgroundColor: '#fff',
      alignItems: 'center',
      height: 42,
    },
    inputWrapper: {
      position: 'relative',
      marginVertical: 16
    }
});