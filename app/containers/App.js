
import React, { Component } from 'react';
import { StatusBar, View, StyleSheet } from 'react-native';
import {
    createStore,
    applyMiddleware
} from 'redux';

import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import reducer from '../reducers';

import Routing from './Routing';

const createStoreWithMiddleware = applyMiddleware(thunk)(createStore);
const store = createStoreWithMiddleware(reducer);

export default class App extends Component {
    render() {
        return (
            <Provider store={store}>
                <View style={styles.container}>
                    <StatusBar backgroundColor="#252f3e" />
                    <Routing />
                </View>
            </Provider>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#faf9f7',
    }
});
