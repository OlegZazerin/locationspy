import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Text,
  TextInput,
  Image,
  TouchableOpacity,
  ToastAndroid
} from 'react-native';

var TimerMixin = require('react-timer-mixin');

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import shallowCompare from 'react-addons-shallow-compare';

import * as directionActions from '../actions/directionActions';
import * as recentActions from '../actions/recentActions';
import _ from "lodash";

import MockLocationManger from '../api/MockLocationManager';

import SlideUpContainer from '../components/UI/SlideUpContainer';

import DirectionForm from '../components/Direction/Form';
import RecentPathsList from '../components/RecentPathsList';
import RecentSearchesList from '../components/RecentSearchesList';
import DirectionsBar from '../components/Direction/DirectionsBar';
import Manager from '../components/Direction/Manager';
import Map from '../components/Direction/Map';
import CurrentStepView from '../components/Direction/CurrentStepView';

import { getDistanceFromLatLonInMeters, getDataStep } from '../utils/utils';

class Direction extends Component {
  constructor(props) {
    super(props);

    this.state = {
      focusRef: '',
      directionSteps: null,
      intervalId: null,
      isShowModalRecent: false,
      isPausedMockDirection: false
    }
  }
  componentWillUnmount () {
    clearInterval(this.state.intervalId);
    MockLocationManger.clearLocation();
  }
  shouldComponentUpdate(nextProps, nextState) {
    return shallowCompare(this.props, nextProps) || shallowCompare(this.state, nextState);
  }
  render() {
    const { location, direction, recent } = this.props;
    const { isShowModalRecent, isPausedMockDirection } = this.state;

    return (
        <View style={styles.container}>
          <Map
            chosenDirection={direction.direction}
            isPausedMockDirection={isPausedMockDirection}
            originalLocation={location.originalLocation}
            mockLocation={direction.mockLocation}
            passedSteps={direction.passedSteps}
            directions={direction.directions}
            isActiveMockLocation={direction.isActiveMockLocation}/>
          <SlideUpContainer visible={isShowModalRecent}>
            <View style={styles.modalContainer}>
              <RecentPathsList 
                data={recent.paths}
                onSelect={this.props.setPath} />
              <RecentSearchesList
                data={recent.searches}
                onSelect={this.setLocation} />
            </View>
          </SlideUpContainer>  
          <DirectionForm
            hide={direction.isActiveMockLocation}
            origin={direction.origin}
            destination={direction.destination}
            onFocus={this.onFocusInput}
            onBlur={this.onBlurInput}
            onSwap={this.props.swap}
            recentSearches={recent.searches}
            onChangeOrigin={this.onChangeOrigin}
            onChangeDestination={this.onChangeDestination}/>            
          <DirectionsBar
            hide={isShowModalRecent || direction.isActiveMockLocation }
            chosenDirection={direction.direction}
            directions={direction.directions}
            onChange={this.props.setDirection}/>  
          <CurrentStepView 
            hide={!direction.isActiveMockLocation}
            streetName={direction.streetName}
            onChangePause={this.pauseMockDirection}/>
          <Manager
            hide={isShowModalRecent}
            direction={direction}
            onChangeAutoDeactivation={this.props.changeAutoDeactivation}
            onChangeActiveState={this.onChangeStateDirection}/>
        </View>
    );
  }
  onFocusInput = focusRef => {
    this.setState({focusRef, isShowModalRecent: true});
  }
  onBlurInput = () => {
    this.setState({ focusRef: '', isShowModalRecent: false})
  }
  onChangeOrigin = data => {
    this.props.addSearches(data);
    this.props.setOrigin(data);
  }
  onChangeDestination = data => {
    this.props.addSearches(data);
    this.props.setDestination(data);
  }
  setLocation = data => {
    const { focusRef } = this.state;
    if(focusRef == 'inputA') {
      this.props.setOrigin(data);
    } else if(focusRef == 'inputB') {
      this.props.setDestination(data);
    }
  } 
  onChangeStateDirection = value => {
    this.props.changeStateDirection(value);

    if(value) {
      this.startMockDirection();
    } else {
      this.stopMockDirection();
    }
  }

  startMockDirection = () => {
    const { currentStep, mockLocation } = this.props.direction;

    if(currentStep == null) return;

    MockLocationManger.addGPSProvider()
    .catch(() => { console.log('provider is not added') });

    this.nextStep(0);

    const intervalId = setInterval(this.nextStep, 1000);

    this.setState({intervalId});
  }

  nextStep = () => {
    const { direction, currentStep, nextStep, mockLocation, autoDeactivation } = this.props.direction;
    const length = currentStep.points.length;
    const index = mockLocation.index+1;
    let coordinate = null;
    let distance = 0;      

    if(index < length) {
      coordinate = currentStep.points[index];
      distance = currentStep.oneStepDistance;
    } else if(nextStep) {
      coordinate = nextStep.points[0];
      distance = nextStep.oneStepDistance;
      this.props.updateCurentStep(nextStep);
      this.props.updateNextStep(getDataStep(direction, nextStep.index+1));
    } else {
      if(autoDeactivation) {
        this.onChangeStateDirection(false);
      } else {
        clearInterval(this.state.intervalId);
      }
      return;
    }

    this.props.updateMockLocation(coordinate);
    MockLocationManger.addNewLocation(coordinate.latitude, coordinate.longitude)
    .catch(() => { console.log('failed to add the coordinates') });
    delete coordinate['index'];
    this.props.addPassedStep(coordinate);
    this.props.addPassedDistance(distance);
    this.updateStreetName();
  }

  pauseMockDirection = value => {
    if(value) {
      clearInterval(this.state.intervalId);
    } else {
      const intervalId = setInterval(this.nextStep, 100);
      this.setState({intervalId});
    }

    this.setState({ isPausedMockDirection: value });
  }

  stopMockDirection = () => {
    const { originalLocation } = this.props.location;

    clearInterval(this.state.intervalId);

    console.log(originalLocation);

    if(originalLocation) {
        MockLocationManger
        .addNewLocation(originalLocation.latitude, originalLocation.longitude);
    }

    MockLocationManger.clearLocation();
    this.props.resetMockDirectionData();
    this.props.initializationStepsRoute(this.props.direction.direction);
  }

  updateStreetName = _.throttle(this.props.updateStreetName, 20000);
}

const styles = StyleSheet.create({
    container: {
        marginTop: 74,
        flex: 1,
        backgroundColor: 'rgb(250, 249, 247)',
        justifyContent: 'space-between',
    },
    map: {
        ...StyleSheet.absoluteFillObject
    },
    modalContainer: {
       flex: 1,
       marginTop: 100,
       backgroundColor: 'rgb(250, 249, 247)'
    }
});

const mapDispatchToProps = dispatch => bindActionCreators({...directionActions, ...recentActions}, dispatch);

const mapStateToProps = ({ direction, location, recent }) => ({ direction, location, recent });

export default connect(mapStateToProps, mapDispatchToProps)(Direction);
