
import React, { Component } from 'react';
import { AppState, StyleSheet, PixelRatio, View, Text } from 'react-native';

import { Scene, Router } from 'react-native-router-flux';

import Search from './Search';
import SelectOnMap from './SelectOnMap';
import Main from './Main';
import AccessManager from './AccessManager';

export default class Routing extends Component {
    render() {
        return (
            <Router>
              <Scene key="accessManager" component={AccessManager} hideNavBar>
              	<Scene key="root" sceneStyle={{backgroundColor: 'red'}} hideNavBar>
                    <Scene key="main" duration={200} component={Main}/>
                    <Scene key="search" duration={200} component={Search}/>
                    <Scene key="selectOnMap" duration={200} component={SelectOnMap}/>
                </Scene>
              </Scene>
            </Router>
        );
    }
}
