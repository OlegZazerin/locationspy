import { ToastAndroid } from 'react-native';
import _ from 'lodash';
import GoogleApi from '../api/GoogleApi';
import MockLocationManger from '../api/MockLocationManager';
import * as utils from '../utils/utils';
const GApi = new GoogleApi();

import { setOrigin } from './directionActions';

export const SET_ORIGINAL_LOCATION = 'SET_ORIGINAL_LOCATION';
export const CHANGE_FLAG_ALLOWED_USE_APP = 'CHANGE_FLAG_ALLOWED_USE_APP';
export const CHANGE_MOCK_COORDINATE = 'CHANGE_MOCK_COORDINATE';
export const CHANGE_LOCATION_DESCRIPTION = 'CHANGE_LOCATION_DESCRIPTION';
export const CHANGE_STATUS_MOCK_LOCATION = 'CHANGE_STATUS_MOCK_LOCATION';

export function changeFlagAllowedUseApp(value) {
    return {
        type: CHANGE_FLAG_ALLOWED_USE_APP,
        value
    }
}

export function setMockLocation({ longitude, latitude }) {
    return (dispatch, getState) => {
        const coordinate = getState().location.mockLocation.coordinate;

        if(coordinate && coordinate.longitude == longitude && coordinate.latitude == latitude) return;

        dispatch(updateDescriptionMockLocation({ longitude, latitude }));
        dispatch({
            type: CHANGE_MOCK_COORDINATE,
            value: { longitude, latitude }
        });
    }
}

export function updateDescriptionMockLocation(coordinate) {
   return (dispatch, getState) => {
       let value = {
            placeId: null,
            description: null
        }

       GApi.geocodeLocation(coordinate)
       .then(res => {
           if(res.status == "OK") {
               value = {
                   placeId: res.results[0].place_id,
                   description: {
                       address: utils.getFormatedAddress(res.results[0].address_components) ,
                       locality: utils.getFormatedLocality(res.results[0].address_components)
                    }
                }
                dispatch(setOrigin(res.results[0]));
           }
           dispatch({
               type: CHANGE_LOCATION_DESCRIPTION,
               value
            });
       })
       .catch(e => {
            console.log(e);
            ToastAndroid.show('Request failed, pleas check the connection to Ethernet! ', 2000)
            dispatch({
               type: CHANGE_LOCATION_DESCRIPTION,
               value
            });
        });
   }
}

export function getLastKnownLocation(value) {
    return (dispatch, getState) => {
        MockLocationManger.getLastKnownLocation()
        .then(position => {
            const coordinate = {
                latitude: position.latitude,
                longitude: position.longitude
            }
            dispatch({
                type: SET_ORIGINAL_LOCATION,
                value: coordinate
            });
            dispatch(setMockLocation(coordinate))
        })
        .catch(e => ToastAndroid.show('Failed to get the gps coordinates', 2000)); 
    }
}

export function changeStatusMockLocation(value) {
    return {
        type: CHANGE_STATUS_MOCK_LOCATION,
        value
    };  
}