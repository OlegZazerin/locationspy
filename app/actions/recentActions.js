import { AsyncStorage } from 'react-native';
import _ from 'lodash';

export const SET_RECENT_DATA = 'SET_RECENT_DATA';
export const UPDATE_PATHS = 'UPDATE_PATHS';
export const UPDATE_SEARCHES = 'UPDATE_SEARCHES';

export function saveRecentData() {
  return (dispatch, getState) => {
    const data = getState().recent;
    AsyncStorage.setItem('@Locationspy:RecentData', JSON.stringify(data));
  }
}

export function loadingRecentData() {
    return (dispatch, getState) => {
      AsyncStorage.getItem('@Locationspy:RecentData', (err, value) => {
        if(value !== null) {
          const data = JSON.parse(value);
          dispatch({
            type: SET_RECENT_DATA,
            data
          });
        }
      });
  }
}

export function addPath(value) {
  return (dispatch, getState) => {
    const paths = getState().recent.paths;
    const index = paths.findIndex(e => _.isEqual(e, value));

    if(index != -1) {
      paths.splice(index, 1);
    }

    paths.reverse();
    paths.push(value);
    paths.reverse();

    dispatch({
      type: UPDATE_PATHS,
      value: paths.slice(0,9)
    });
    dispatch(saveRecentData());
  }
}

export function addSearches(value) {
  return (dispatch, getState) => {
    const searches = getState().recent.searches;
    const index = searches.findIndex(e => _.isEqual(e, value));

    if(index != -1) {
      searches.splice(index, 1);
    }

    searches.reverse();
    searches.push(value);
    searches.reverse();

    dispatch({
      type: UPDATE_SEARCHES,
      value: searches.slice(0,9)
    });
    dispatch(saveRecentData());
  }
}
