import GoogleApi from '../api/GoogleApi';
import { addPath } from './recentActions';
import polyline from 'polyline';
import _ from 'lodash';
import { 
  getDistanceFromLatLonInMeters, 
  getDataStep, 
  getFormatedAddress, 
  getFormatedLocality,
  extractFromAdress 
} from '../utils/utils';
import { ToastAndroid } from 'react-native';
const GApi = new GoogleApi();

export const SET_ACTUAL_LOCATION = 'SET_ACTUAL_LOCATION';
export const SET_ORIGIN = 'SET_ORIGIN';
export const SET_DESTINATION = 'SET_DESTINATION';
export const SET_PATH = 'SET_PATH';
export const ADD_DIRECTIONS = 'ADD_DIRECTIONS';
export const ACTIVE_DIRECTION = 'ACTIVE_DIRECTION';
export const LOADING_PATHS_START = 'LOADING_PATHS_START';
export const LOADING_PATHS_SUCCESS = 'LOADING_PATHS_SUCCESS';
export const LOADING_PATHS_ERROR = 'LOADING_PATHS_ERROR';
export const CHANGE_AUTO_DEACTIVATION = 'CHANGE_AUTO_DEACTIVATION';
export const SET_DIRECTION = 'SET_DIRECTION';
export const SWAP_ORIGIN_AND_DESTINATION = 'SWAP_ORIGIN_AND_DESTINATION';


export const SET_CURRENT_STEP = 'SET_CURRENT_STEP';
export const SET_DESCRIPTION_CURRENT_STEP = 'SET_DESCRIPTION_CURRENT_STEP';
export const SET_NEXT_STEP ='SET_NEXT_STEP';
export const SET_COORDINATE_MOCK_LOCATION = 'SET_COORDINATE_MOCK_LOCATION';
export const ADD_TO_PASSED_DISTANCE = 'ADD_TO_PASSED_DISTANCE';
export const ADD_PASSED_STEP = 'ADD_PASSED_STEP';
export const RESET_MOCK_DIRECTION_DATA = 'RESET_MOCK_DIRECTION_DATA';
export const SET_STREET_NAME = 'SET_STREET_NAME';

let intervalId = null; 

export function setOrigin(value) {
    return (dispatch, getState) => {
      const destination = getState().direction.destination;

      dispatch({
        type: SET_ORIGIN,
        value
      });

      if(destination) {
        dispatch(getDirections(value, destination));
      }
    }
}

export function setDestination(value) {
    return (dispatch, getState) => {
      const origin = getState().direction.origin;

      dispatch({
        type: SET_DESTINATION,
        value
      });

      if(origin) {
        dispatch(getDirections(origin, value));
      }
    }
}

export function getDirections(origin, destination) {
  return (dispatch, getState) => {
    const types = ['driving', 'walking', 'bicycling'];
    let directions = [];

    dispatch({type: LOADING_PATHS_START});
    dispatch(addPath({origin, destination}));
    
    const pAll = types.map(type => GApi.getDirections(origin.place_id, destination.place_id, type)
        .then(res => {
          if(res.status = "OK" && res.routes.length) {
            directions.push({ type, data: res.routes[0].legs[0] });
          } else {
            console.log(res.status);
          }
        })
        .catch(err => { console.log(err.toString()) })
    );

    Promise.all(pAll).then(() => {
      console.log(directions);
      dispatch({ 
        type: ADD_DIRECTIONS, 
        value: _.sortBy(directions, ['type'])
      });

      if(directions != null && directions.length > 0) {
        dispatch(setDirection(directions[0]));
      } else {
        ToastAndroid.show('Routes list is empty! ', 2000)
      }
    	
    }).catch(err => {
    	console.log('Catch: ', err);
    });
  }
}

export function changeAutoDeactivation(value) {
  return {
    type: CHANGE_AUTO_DEACTIVATION,
    value
  }
}

export function setDirection(value) {
  return (dispatch, getState) => {

    dispatch(initializationStepsRoute(value));

    dispatch({
      type: SET_DIRECTION,
      value
    });
  }
}

export function setPath({origin, destination}) {
  return (dispatch, getState) => {
    dispatch({
      type: SET_PATH,
      origin,
      destination
    });
    dispatch(getDirections(origin, destination));
  }
}

export function swap() {
  return (dispatch, getState) => {
    const { origin, destination } = getState().direction;
    dispatch({
      type: SWAP_ORIGIN_AND_DESTINATION,
    });
    if(origin && destination) {
      dispatch(getDirections(destination, origin));
    }
  }
}

export function changeStateDirection(value) {
  return (dispatch, getState) => {
    dispatch({
      type: ACTIVE_DIRECTION,
      value
    });
  }
}



export function initializationStepsRoute(direction) {
  return (dispatch, getState) => {

    if(direction == null) return;

    const currentStep = getDataStep(direction, 0);
    const nextStep = getDataStep(direction, 1);

    dispatch(updateCurentStep(currentStep));
    dispatch(updateNextStep(nextStep));
  }
}

export function updateMockLocation(value) {
  return(dispatch, getState) => {

    dispatch({
      type: SET_COORDINATE_MOCK_LOCATION,
      value
    });
  }
}

export function updateStreetName() {
  return(dispatch, getState) => {
    const coordinate = getState().direction.mockLocation.coordinate;
    let value = '';

    if(coordinate == null) return;

    GApi.geocodeLocation(coordinate)
    .then(res => {
        if(res.status == "OK") {
            console.log(res.results[0].address_components);
            value = extractFromAdress(res.results[0].address_components, "route");
        }
        dispatch({
            type: SET_STREET_NAME,
            value
        });
    })
    .catch(e => {
        console.log(e);
        ToastAndroid.show('Request failed, pleas check the connection to Ethernet! ', 2000)
        dispatch({
            type: SET_STREET_NAME,
            value
        });
    });
  }
}

export function updateCurentStep(value) {
  return (dispatch, getState) => {
    dispatch({
        type: SET_CURRENT_STEP,
        value
    });
  }
}

export function updateNextStep(value) {
  return {
      type: SET_NEXT_STEP,
      value
  }
}

export function addPassedStep(value) {
  return {
      type: ADD_PASSED_STEP,
      value
    }
}

export function addPassedDistance(value) {
  return {
      type: ADD_TO_PASSED_DISTANCE,
      value
    }
}

export function resetMockDirectionData() { 
  return {
      type: RESET_MOCK_DIRECTION_DATA
    }
}