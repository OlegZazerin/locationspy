export const SET_REGION = 'SET_REGION';
export const CHANGE_MARKERS = 'CHANGE_MARKERS';
export const ADD_DIRECTION = 'ADD_DIRECTION';
export const REMOVE_DIRECTION = 'REMOVE_DIRECTION';

export function setRegion(value) {
    return {
        type: SET_REGION,
        value
    }
}

export function addMarker(value) {
    return (dispatch, getState) => {
        let markers = getState().map.markers;
        markers.push(value);
        dispatch(changeMarkers(markers));
    }
}

export function removeMarker(id) {
    return (dispatch, getState) => {

    }
}

export function clearMarkers(value) {
    return (dispatch, getState) => {
        dispatch(changeMarkers([]));
    }
}

export function removeCircleFromMarker(id) {
    return (dispatch, getState) => {
        const markers = getState().map.markers;
        const index = markers.findIndex(e => e.id == id);

        if(index != -1) {
           delete markers[index].circle;
        }

        dispatch(changeMarkers(markers));
    }
}

// export function setCoordinateForMarker(id, event) {
//     return (dispatch, getState) => {
//         const markers = getState().map.markers;
//         if(markers[id]) {
//             markers[id].coordinate = event.coordinate;
//         }
//         dispatch(changeMarkers(markers));
//     }
// }

export function changeMarkerOptions(id, options) {
  return (dispatch, getState) => {
    const markers = getState().map.markers;
    const index = markers.findIndex(e => e.id == id);

    if(index != -1) {
      markers[index] = Object.assign(markers[index], options);
    }

    dispatch(changeMarkers(markers));
  }
}

export function changeMarkers(value) {
    return {
        type: CHANGE_MARKERS,
        value
    }
}

// export function setMockCoordinate(value) {
//     return {
//         type: ADD_DIRECTION,
//         value
//     }
// }
//
// export function addDirection(value) {
//     return {
//         type: ADD_DIRECTION,
//         value
//     }
// }
//
// export function removeDirection(value) {
//     return {
//         type: REMOVE_DIRECTION,
//         value
//     }
// }
