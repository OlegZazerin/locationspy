import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    Text,
    TextInput,
    Image,
    TouchableOpacity,
    ListView
} from 'react-native';

import { getFormatedAddress, getFormatedLocality } from '../utils/utils';

const recentIcon = require('../components/img/recent.png');

class RecentSearchesItemView extends Component {
    render() {
        const { data } = this.props;

        return (
            <TouchableOpacity activeOpacity={0.7} onPress={() => { this.props.onPress(data) }}>
                <View style={styles.wrapper}>
                    <Image source={recentIcon} style={styles.icon}/>
                    <View style={styles.textWrapper}>
                        <Text style={styles.address}>{getFormatedAddress(data.address_components)}</Text>
                        <Text style={styles.city}>{getFormatedLocality(data.address_components)}</Text>
                    </View>
                </View>
            </TouchableOpacity>
        );
    }
}

export default RecentSearchesItemView;

const styles = StyleSheet.create({
    wrapper: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    textWrapper: {
        flex: 1,
        flexDirection: 'row',
        borderBottomWidth: 1,
        borderBottomColor: 'rgba(65, 77, 94, 0.09)',
        paddingVertical: 20,
        alignSelf: 'stretch'
    },
    address: {
        fontFamily: 'AvenirNextLTPro-Demi',
        fontSize: 14,
        lineHeight: 18,
        color: 'rgb(56, 69, 87)'
    },
    city: {
        fontFamily: 'AvenirNextLTPro-Demi',
        fontSize: 13,
        lineHeight: 17,
        color: 'rgba(57, 70, 88, 0.4)',
        marginLeft: 10
    },
    icon: {
        marginHorizontal: 15,
        width: 23,
        height: 20
    }
});
