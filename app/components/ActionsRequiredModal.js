import React, { Component } from 'react';
import { AppState, PixelRatio, View, Text, StyleSheet, Image } from 'react-native';

import Button from '../components/UI/Button';

class StatusIconView extends Component {
    render() {
        const { status, title } = this.props;

        switch (status) {
            case 'confirmed':
                return <Image source={require('../components/img/checkmark.png')} style={styles.checkmarkIcon} />
            case 'rejected':
                return <Image source={require('../components/img/close.png')} style={styles.closeIcon} />
            case 'showTitles':
            default:
                return <Text style={styles.modalContentItemTitleText}>{title}</Text>
        }
    }
}

export default class ActionsRequiredModal extends Component {
    render() {
        const { isDebuggable, isAllowedMockLocations } = this.props;
        const modeItems = !isDebuggable && !isAllowedMockLocations ? 'showTitles' : '';

        return (
			<View style={styles.modalWrapper}>
				<View style={styles.modalContainer}>
					<View style={styles.modalHeader}>
						<Text style={styles.modalHeaderTitle}>Few more actions required</Text>
					</View>
					<View style={styles.modalContent}>
						<View style={styles.modalContentItemWrapper}>
							<View style={styles.modalContentItemTitleWrapper}>
								<StatusIconView status={modeItems || (isDebuggable ? "confirmed" : "rejected")} title="1"/>
							</View>
							<View style={styles.modalContentItemContent}>
								<Text style={styles.modalContentItemHeader}>Enable Developer options</Text>
								<Text style={styles.modalContentItemText}>Settings > About Phone ></Text>
								<Text style={styles.modalContentItemText}>Tap 10 times on Build Number</Text>
							</View>
						</View>
						<View style={styles.modalContentItemWrapper}>
							<View style={styles.modalContentItemTitleWrapper}>
								<StatusIconView status={modeItems || (isAllowedMockLocations ? "confirmed" : "rejected")} title="2"/>
							</View>
							<View style={[styles.modalContentItemContent, styles.borderHide]}>
								<Text style={styles.modalContentItemHeader}>Set Locationspy as a mock location app</Text>
								<Text style={styles.modalContentItemText}>Settings > Developer options ></Text>
								<Text style={styles.modalContentItemText}>Select mock location</Text>
							</View>
						</View>
					</View>
					<View style={styles.modalActions}>
						<Button
							disabled={!isDebuggable || !isAllowedMockLocations}
                            styleWrapper={{borderBottomLeftRadius: 4, borderBottomRightRadius: 4}}
							onPress={this.props.onPress}
							title="Got it"/>
					</View>
				</View>
			</View>
        );
    }
}

const styles = StyleSheet.create({
    modalWrapper: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        backgroundColor: 'rgba(0,0,0,.5)',
        justifyContent: 'center',
        alignItems: 'center'
    },
    modalHeader:{
        borderBottomWidth: 1,
        alignItems: 'center',
        justifyContent: 'center',
        borderColor: 'rgba(255, 255, 255, 0.04)',
        paddingVertical: 18
    },
    modalHeaderTitle: {
        fontFamily: 'AvenirNextLTPro-Bold',
        fontSize: 18,
        color: '#fff',
        lineHeight: 21
    },
    modalContainer: {
        width: 280,
        backgroundColor: 'rgba(47, 60, 79, 0.95)',
        borderWidth: 0,
        borderRadius: 5
    },
    modalContentItemWrapper: {
        flexDirection: 'row',
        paddingTop: 24
    },
    modalContentItemTitleWrapper:{
        width: 38,
        height: 38,
        justifyContent: 'center',
        alignItems: 'center',
        marginHorizontal: 8
    },
    modalContentItemTitleText:{
        fontFamily: 'AvenirNextLTPro Medium',
        fontSize: 38,
        lineHeight: 42,
        color: '#f9dc5c'
    },
    modalContentItemContent:{
        flex: 1,
        flexWrap: 'wrap',
        alignSelf: 'stretch',
        borderBottomWidth: 1,
        borderColor: 'rgba(255, 255, 255, 0.04)',
        paddingBottom: 24
    },
    modalContentItemHeader:{
        fontFamily: 'AvenirNextLTPro Demi',
        fontSize: 15.5,
        lineHeight: 18,
        color: '#fff',
        marginBottom: 5
    },
    modalContentItemText:{
        fontFamily: 'AvenirNextLTPro Demi',
        fontSize: 13,
        lineHeight: 17,
        color: 'rgba(255, 255, 255, 0.702)'
    },
    checkmarkIcon: {
        width: 25,
        height: 18
    },
    closeIcon: {
        width: 23,
        height: 23
    }
});
