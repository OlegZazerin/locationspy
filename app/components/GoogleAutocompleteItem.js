
import React, { Component } from 'react';
import { TouchableOpacity, View, Text, StyleSheet, Image } from 'react-native';

const GPlaceMarkerImg = require('./img/g_place_marker.png');
const RecentImg = require('./img/recent.png');

export default class GoogleAutocompleteItem extends Component {
    render(){
        const { data, isRecent, maxWidthContent } = this.props;
        const dLenght = data.description.lenght;
        let mainText = data.structured_formatting.main_text;
        let secondaryText = data.structured_formatting.secondary_text;
        let icon = <Image source={GPlaceMarkerImg} style={styles.iconGPlaceMarker} />

        if(isRecent) {
          icon = <Image source={RecentImg} style={styles.iconRecently} />
        }

        return (
          <TouchableOpacity activeOpacity={0.7} onPress={() => { this.props.onPress(data) }}>
                <View style={styles.wrapper}>
                    <View style={styles.icon}>
                        { icon }
                    </View>
                    <View style={styles.content}>
                        <Text style={styles.address}>{mainText}</Text>
                        <Text style={styles.city}>{secondaryText}</Text>
                    </View>
                </View>
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    wrapper: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    content: {
        flex: 1,
        flexDirection: 'row',
        borderBottomWidth: 1,
        borderBottomColor: 'rgba(65, 77, 94, 0.09)',
        paddingVertical: 20,
        alignSelf: 'stretch'
    },
    address: {
        fontFamily: 'AvenirNextLTPro-Demi',
        fontSize: 15,
        lineHeight: 18,
        color: 'rgb(56, 69, 87)'
    },
    city: {
        fontFamily: 'AvenirNextLTPro-Demi',
        fontSize: 14,
        lineHeight: 17,
        color: 'rgba(57, 70, 88, 0.4)',
        marginLeft: 10
    },
    icon: {
        marginHorizontal: 15,
        width: 20,
        alignItems: 'center'
    }
});
