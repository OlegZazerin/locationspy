import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    Text,
    ListView
} from 'react-native';

import _ from 'lodash';
import RecentSearchesItemView from './RecentSearchesItemView';

const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

class RecentSearchesList extends Component {
    constructor(props) {
        super(props);
        const data = Array.prototype.slice.call(props.data) || [];
        this.state = {
            dataSource: ds.cloneWithRows(data),
        };
    }
    componentWillReceiveProps(nextProps) {
      const data = Array.prototype.slice.call(nextProps.data);
      const dataSource = ds.cloneWithRows(data);
      this.setState({dataSource});
    }
    shouldComponentUpdate(nextProps, nextState) {
        return !_.isEqual(nextProps, this.props) || !_.isEqual(nextState, this.state);
    }
    render() {
        console.log('re-render RecentSearchesList');
        return (
            <View style={styles.wrapper}>
                <Text style={styles.title}>RECENT SEARCHES</Text>
                <View style={styles.content}>
                  <ListView
                    enableEmptySections={true}
                    dataSource={this.state.dataSource}
                    renderRow={rowData =>
                      <RecentSearchesItemView
                        data={rowData}
                        onPress={this.props.onSelect} />
                    }
                  />
              </View>
            </View>
        );
    }
}

export default RecentSearchesList;

const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
        marginTop: 29,
        marginHorizontal: 24,
        marginBottom: 15,
    },
    title: {
        fontFamily: 'AvenirNextLTPro-Bold',
        fontSize: 13,
        lineHeight: 18,
        color: 'rgba(58, 70, 88, 0.502)',
        marginBottom: 15
    },
    content: {
        flex: 1,
        borderRadius: 4,
        elevation: 4,
        backgroundColor: '#fff'
    }
});
