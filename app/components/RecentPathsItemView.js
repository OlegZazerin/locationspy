import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    Text,
    TextInput,
    Image,
    TouchableOpacity
} from 'react-native';

import { getFormatedAddress, getFormatedLocality } from '../utils/utils';

class RecentPathsItemView extends Component {
    render() {
        const { data } = this.props;

        if(!data.origin || !data.destination) return null;

        return (
            <TouchableOpacity activeOpacity={0.7} onPress={() => { this.props.onPress(data) }}>
                <View style={styles.recentPathsContentItem}>
                    <View style={styles.pathsItemIconContainer}>
                      <View style={{flex: 1, width: 11, justifyContent: 'flex-end', backgroundColor: 'transparent'}}>
                        <View style={{width: 11, height: 11, borderRadius: 11, backgroundColor: 'rgb(249, 220, 92)'}}/>
                      </View>
                      <View style={{flex: 2, width: 1.5, overflow : 'hidden', position: 'relative', backgroundColor: 'transparent'}}>
                        <View style={{flex: 1, width: 3, borderWidth: 4.8, borderColor: '#fff', borderStyle: 'dotted', backgroundColor: 'transparent'}}/>
                      </View>
                      <View style={{flex: 1, width: 11, justifyContent: 'flex-start', backgroundColor: 'transparent'}}>
                        <View style={{width: 11, height: 11, borderRadius: 11, backgroundColor: 'rgb(249, 220, 92)'}}/>
                      </View>
                    </View>
                    <View>
                        <View style={styles.pathsItemContainer}>
                          <Text style={styles.address}>{getFormatedAddress(data.origin.address_components)}</Text>
                          <Text style={styles.city}>{getFormatedLocality(data.origin.address_components)}</Text>
                        </View>
                        <View style={styles.pathsItemContainer}>
                          <Text style={styles.address}>{getFormatedAddress(data.destination.address_components)}</Text>
                          <Text style={styles.city}>{getFormatedLocality(data.destination.address_components)}</Text>
                        </View>
                    </View>
                </View>
            </TouchableOpacity>
        );
    }
}

export default RecentPathsItemView;

const styles = StyleSheet.create({
    recentPathsContentItem: {
        backgroundColor: 'rgb(66, 78, 95)',
        borderRadius: 4,
        marginRight: 12,
        flexDirection: 'row'
    },
    pathsItemContainer: {
        marginVertical: 13,
        marginRight: 37
    },
    pathsItemIconContainer: {
        marginVertical: 13,
        marginHorizontal: 16,
        justifyContent: 'space-around',
        alignItems: 'center'
    },
    address: {
        fontFamily: 'AvenirNextLTPro-Demi',
        fontSize: 16,
        color: '#fff',
        lineHeight: 21
    },
    city: {
        fontFamily: 'AvenirNextLTPro-Demi',
        fontSize: 13,
        color: 'rgba(255, 255, 255, 0.4)',
        lineHeight: 16
    }
});
