import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    Text,
    ListView
} from 'react-native';

import _ from 'lodash';
import RecentPathsItemView from './RecentPathsItemView'

const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

class RecentPathsList extends Component {
    constructor(props) {
        super(props);
        const data = Array.prototype.slice.call(props.data) || [];
        this.state = {
            dataSource: ds.cloneWithRows(data),
        };
    }
    componentWillReceiveProps(nextProps) {
      const data = Array.prototype.slice.call(nextProps.data);
      const dataSource = ds.cloneWithRows(data);
      this.setState({dataSource});
    }
    shouldComponentUpdate(nextProps, nextState) {
        return !_.isEqual(nextProps, this.props) || !_.isEqual(nextState, this.state);
    }
    render() {
        console.log('re-render RecentPathsList');
        return (
            <View style={styles.recentPathsWrapper}>
                <Text style={styles.recentPathsTitle}>RECENT PATHS</Text>
                <ListView
                  enableEmptySections={true}
                  contentContainerStyle={styles.recentPathsContent}
                  horizontal={true}
                  showsHorizontalScrollIndicator={false}
                  dataSource={this.state.dataSource}
                  renderRow={rowData =>
                    <RecentPathsItemView
                      data={rowData}
                      onPress={this.props.onSelect} />
                  }
                />
            </View>
        );
    }
}

export default RecentPathsList;

const styles = StyleSheet.create({
    recentPathsWrapper: {
        marginTop: 29,
    },
    recentPathsTitle: {
        fontFamily: 'AvenirNextLTPro-Bold',
        fontSize: 13,
        lineHeight: 18,
        color: 'rgba(58, 70, 88, 0.502)',
        paddingLeft: 20
    },
    recentPathsContent: {
        flexDirection: 'row',
        paddingTop: 15,
        paddingLeft: 20,
        height: 150
    }
});
