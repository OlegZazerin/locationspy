import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  TouchableNativeFeedback
} from 'react-native';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

export default class Button extends Component {
    render() {
      const { text, source, hide, styleIcon, onPress } = this.props;
      let icon = null;

      if(source) {
        icon = <Image source={source} style={styleIcon || styles.icon}/>
      }

      if(hide) return null;

      return (
          <TouchableNativeFeedback
            onPress={() => {
              if(typeof onPress === 'function') {
                onPress();
              }
            }}>
            <View style={styles.container}>
              {icon}
              <Text style={styles.text}>{this.props.text || ''}</Text>
            </View>
          </TouchableNativeFeedback>
      );
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      flexDirection: 'row',
      backgroundColor: 'rgb(66, 78, 95)',
      borderRadius: 4,
      height: 50,
      alignItems: 'center',
      justifyContent: 'center',
      marginHorizontal: 4
    },
    text: {
      marginLeft: 12,
      fontFamily: 'AvenirNextLTPro-Demi',
      fontSize: 16,
      color: '#fff',
      lineHeight: 18
    },
    icon: {
      width: 20,
      height: 20
    }
});
