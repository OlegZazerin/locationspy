import React, { Component } from 'react';
import { AppState, PixelRatio, View, Text, StyleSheet, Image } from 'react-native';
import { getFormatedAddress, getFormatedLocality } from '../../utils/utils';
import Button from '../UI/Button';
import { Actions } from 'react-native-router-flux';

export default class InfoBar extends Component {
    render() {
        const { coordinate, description, onPressOk, onPressCancel } = this.props;
        let coordinateText =  '/';

        if(coordinate) {
            coordinateText = `N ${coordinate.latitude.toFixed(4)}° / W ${coordinate.longitude.toFixed(4)}°`
        }

        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <Text style={styles.coordinate}>{coordinateText}</Text>
                </View>
                <View style={styles.body}>
                    <Text style={styles.address}>{ description ? description.address : ' ' }</Text>
                    <Text style={styles.city}>{ description ? description.locality : ' ' }</Text>
                </View>
                <View style={styles.actions}>
                    <Button
                        title="ok"
                        styleWrapper={{borderBottomLeftRadius: 4, flex: 1}}
                        backgroundColor='rgb(249, 220, 92)'
                        onPress={() => {
                            if(typeof onPressOk === 'function' ) {
                                const data = {
                                    latitude: coordinate.latitude,
                                    longitude: coordinate.longitude
                                }
                                onPressOk(data);
                            }
                        }} />
                    <Button
                        title="Cancel"
                        styleWrapper={{borderBottomRightRadius: 4, flex: 1}}
                        backgroundColor='rgb(80, 110, 134)'
                        color={'#fff'}
                        onPress={() => {
                            if(typeof onPressCancel === 'function' ) {
                                onPressCancel();
                            }
                        }} />
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        width: 250,
        marginBottom: 10,
        borderWidth: 0,
        borderRadius: 5,
        backgroundColor: 'rgba(47, 60, 79, 0.95)',
        elevation: 1,
        zIndex: 1,
        alignSelf: 'center'
    },
    header: {
        borderBottomWidth: 1,
        borderColor: 'rgba(255, 255, 255, 0.04)',
        justifyContent: 'center',
        alignItems: 'center',
        paddingVertical: 12.5
    },
    coordinate: {
        fontFamily: 'AvenirNextLTPro-Demi',
        fontSize: 13,
        color: 'rgba(255, 255, 255, 0.4)'
    },
    body:{
        justifyContent: 'center',
        alignItems: 'center',
        paddingVertical: 20
    },
    address: {
        fontFamily: 'AvenirNextLTPro-Bold',
        fontSize: 17,
        color: '#fff',
        lineHeight: 21,
        textAlign: 'center'
    },
    city: {
        fontFamily: 'AvenirNextLTPro-Demi',
        fontSize: 13,
        color: '#fff',
        lineHeight: 16,
        textAlign: 'center'
    },
    actions: {
        flexDirection: 'row',
        alignSelf: 'stretch'
    }
});
