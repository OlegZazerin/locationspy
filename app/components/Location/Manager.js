import React, { Component } from 'react';
import { AppState, PixelRatio, View, Text, StyleSheet, Image } from 'react-native';
import { getDistanceFromLatLonInMeters } from '../../utils/utils';
import Button from '../UI/Button';

export default class LocationManager extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isAllowedActiveMockLocation: false
        };
    }
    componentWillReceiveProps(nextProps) {
        const { mockLocation, originalLocation } = nextProps;
        if(mockLocation.coordinate && originalLocation) {
            const diff = getDistanceFromLatLonInMeters(
                mockLocation.coordinate.latitude,
                mockLocation.coordinate.longitude,
                originalLocation.latitude,
                originalLocation.longitude
            );
            if(diff > 30) {
                this.setState({isAllowedActiveMockLocation: true});
            } else {
                this.setState({isAllowedActiveMockLocation: false});
            }
        } else {
            this.setState({isAllowedActiveMockLocation: true});
        }
    }
    render() {
        const { show , mockLocation} = this.props;
        let address = ' ', locality = ' ', coordinateText =  '/';

        if(mockLocation.coordinate) {
            coordinateText = `N ${mockLocation.coordinate.latitude.toFixed(4)}° / W ${mockLocation.coordinate.longitude.toFixed(4)}°`
        }

        if(mockLocation.description) {
            address = mockLocation.description.address;
            locality = mockLocation.description.locality;
        }

        if(show) {
            return (
                <View style={styles.container} removeClippedSubviews={true}>
                    <View style={styles.header}>
                        <Text style={styles.coordinate}>{coordinateText}</Text>
                    </View>
                    <View style={styles.body}>
                        <Text style={styles.address}>{address}</Text>
                        <Text style={styles.city}>{locality}</Text>
                    </View>
                    <View style={styles.actions}>
                        { this._renderButton() }
                    </View>
                </View>
            );
        } else {
            return null;
        }
    }
    _renderButton() {
        const { isAllowedActiveMockLocation } = this.state;
        const { isActiveMockLocation } = this.props;

        if(isAllowedActiveMockLocation) {
            if(isActiveMockLocation) {
                return <Button
                            title="DEACTIVATE"
                            styleWrapper={{borderBottomLeftRadius: 4, borderBottomRightRadius: 4}}
                            backgroundColor={'rgb(244, 96, 54)'}
                            color={'#fff'}
                            onPress={() => { this.props.onPress(!isActiveMockLocation) }} />
            } else {
                return <Button
                            title="ACTIVATE LOCATIONSPY"
                            styleWrapper={{borderBottomLeftRadius: 4, borderBottomRightRadius: 4}}
                            onPress={() => { this.props.onPress(!isActiveMockLocation) }} />
            }
        } else {
            return null;
        }
    }
}

const styles = StyleSheet.create({
    container: {
        width: 250,
        marginBottom: 10,
        borderWidth: 0,
        borderRadius: 5,
        backgroundColor: 'rgba(47, 60, 79, 0.95)',
        elevation: 1,
        zIndex: 1,
        overflow: 'hidden',
        alignSelf: 'center'
    },
    header: {
        borderBottomWidth: 1,
        borderColor: 'rgba(255, 255, 255, 0.04)',
        justifyContent: 'center',
        alignItems: 'center',
        paddingVertical: 12.5
    },
    coordinate: {
        fontFamily: 'AvenirNextLTPro-Demi',
        fontSize: 13,
        color: 'rgba(255, 255, 255, 0.4)'
    },
    body:{
        justifyContent: 'center',
        alignItems: 'center',
        paddingVertical: 20
    },
    address: {
        fontFamily: 'AvenirNextLTPro-Bold',
        fontSize: 17,
        color: '#fff',
        lineHeight: 21,
        textAlign: 'center'
    },
    city: {
        fontFamily: 'AvenirNextLTPro-Demi',
        fontSize: 13,
        color: '#fff',
        lineHeight: 16,
        textAlign: 'center'
    },
    actions: {
        alignSelf: 'stretch'
    }
});
