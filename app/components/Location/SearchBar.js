
import React, { Component } from 'react';
import { View, StyleSheet, Text, Image, TouchableNativeFeedback } from 'react-native';

const searchIcon = require('../img/search.png');

export default class SearchBar extends Component {
    render(){
        const { hold, onPress } = this.props;
        
        return (
            <View style={styles.wrapper}>
              <TouchableNativeFeedback
                style={{backgroundColor: 'red'}}
                disabled={hold}
                onPress={() => {
                    if(typeof onPress === 'function') {
                        onPress();
                    }
                }}>
                <View style={[styles.inputContainer, hold && styles.holdConteiner]}>
                  <Image source={searchIcon} style={styles.searchIcon} />
                  <Text style={styles.input}> Search: Coordinates, Countries, Adresses...</Text>
                </View>
              </TouchableNativeFeedback>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    wrapper: {
        marginTop: 74,
        backgroundColor: 'rgba(47, 60, 79, 0.95)',
        paddingHorizontal: 24,
        zIndex: 1,
        paddingBottom: 18
    },
    inputContainer: {
        flexDirection: 'row',
        borderRadius: 4,
        backgroundColor: '#fff',
        height: 42,
        alignItems: 'center'
    },
    input: {
        flex: 1,
        fontFamily: 'AvenirNextLTPro-Demi',
        fontSize: 13,
        lineHeight: 17,
        marginHorizontal: 2,
        color: 'rgb(94, 107, 122)'
    },
    searchIcon: {
        marginLeft: 14,
        marginRight: 6,
        width: 18,
        height: 18
    },
    holdConteiner: {
      backgroundColor: 'rgb(59, 78, 99)'
    }
});
