
import React, { Component } from 'react';
import { Animated, View, StyleSheet, Image, Text, TouchableNativeFeedback  } from 'react-native';
import _ from 'lodash';

export default class CurrentStepView extends Component {
    constructor(props) {
      super(props);

      this.state = {
        state: false
      }
    }
    shouldComponentUpdate(nextProps, nextState) {
      return !_.isEqual(this.props, nextProps) || !_.isEqual(this.state, nextState);
    }
    render() {
      const { hide, streetName } = this.props;
      const { state } = this.state;
      console.log('re-render CurrentStepView');
      if(hide) return null;

      return (
        <View style={styles.container}>
          <View style={styles.addressWrapper}>
            <View style={styles.iconCircle1}>
              <View style={styles.iconCircle2} />
            </View>
            <Text style={styles.title}>{ streetName || '' }</Text>
          </View>
          <TouchableNativeFeedback onPress={this.onChange}>
            <View style={styles.buttonWrapper}>
              <Text style={styles.buttonText}> {state ? 'GO' : 'STAY HERE'} </Text>
            </View>
          </TouchableNativeFeedback>
        </View>
      );

    }
    onChange = value => {
      const { onChangePause } = this.props;
      
      if(typeof onChangePause === 'function') {
        onChangePause(!this.state.state);  
      }
      this.setState({state: !this.state.state});
    }
}

const styles = StyleSheet.create({
    container: {
      position: 'absolute',
      top: 0,
      left:0,
      right:0,
      height: 60,
      alignItems: 'center',
      justifyContent: 'space-between',
      flexDirection: 'row',
      paddingHorizontal: 24,
      backgroundColor:'rgba(255, 255, 255, 0.949)',
    },
    addressWrapper: {
      flexDirection: 'row',
    },
    title: {
      marginLeft: 24,
      fontFamily: 'AvenirNextLTPro-Demi',
      color: 'rgb(50, 63, 83)',
      fontSize: 15,
      lineHeight: 17,
    },
    buttonWrapper: {
      width: 100,
      height: 30,
      backgroundColor: 'rgb(50, 63, 83)',
      borderRadius: 4,
      justifyContent: 'center',
      alignItems: 'center'
    },
    buttonText: {
      fontSize: 10,
      fontFamily: 'AvenirNextLTPro-Bold',
      color: '#fff'
    },
    iconCircle1: {
       width: 15, 
       height: 15, 
       borderRadius: 15, 
       backgroundColor: '#354358', 
       justifyContent: 'center', 
       alignItems: 'center'
    },
    iconCircle2: {
      width: 9, 
      height: 9, 
      borderRadius: 9, 
      backgroundColor: '#f9dc5c'
    }
});
