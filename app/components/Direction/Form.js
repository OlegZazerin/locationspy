import React, {Component} from 'react';
import {
  StyleSheet,
  Modal,
  View,
  Text,
  TextInput,
  Image,
  TouchableOpacity,
  ListView
} from 'react-native';
import shallowCompare from 'react-addons-shallow-compare';
import _ from 'lodash';

import GoogleAPI from '../../api/GoogleApi';
const GoogleApi = new GoogleAPI('AIzaSyD7f1KjELWTkxKf0z44euJVMZjKkSuANGM');

import GooglePlacesAutocomplete from '../GooglePlacesAutocomplete';
import GoogleAutocompleteItem from '../GoogleAutocompleteItem';

const reverseIcon = require('../img/reverse.png');

export default class Form extends Component {
    constructor(props) {
        super(props);

        this.state = {
          origin: null,
          destination: null,
          focusRef: 'inputB',
          queryParameters: {
             key: 'AIzaSyD7f1KjELWTkxKf0z44euJVMZjKkSuANGM',
             language: 'en',
             types: 'address'
          },
          inputStyles: {
            textInputContainer: styles.inputContainer,
            textInput: styles.input,
            separator: styles.separator,
            poweredContainer: {height: 0},
            powered: {height: 0},
            listView: styles.listView
          }
        };
    }
    componentDidMount() {
      this._updateFields(this.props, this.state);
      this.props.onFocus(this.state.focusRef);
    }
    componentWillReceiveProps(nextProps) {
      this._updateFields(nextProps, this.state);
    }
    shouldComponentUpdate(nextProps, nextState) {
      return !_.isEqual(this.props, nextProps) || !_.isEqual(this.state, nextState);
    }
    render() {
      const { focusRef, queryParameters, inputStyles } = this.state;
      const { hide, recentSearches } = this.props;
       console.log('re-render Form');
      if(hide) return <View />;
     
      return (
        <View style={{position: 'relative'}}>
          <View style={{position: 'absolute', top: 0, left: 0, right: 0, height: 110, backgroundColor: 'rgba(47, 60, 79, 0.95)'}} />
          <View style={{position: 'absolute', top: 0, left: 22, right: 22, flexDirection: 'row', alignItems: 'center'}}>
              <View style={styles.inputWrapper}>
                <View>
                    <GooglePlacesAutocomplete
                      ref="inputA"
                      placeholder='Enter origin place'
                      minLength={2}
                      autoFocus={false}
                      fetchDetails={true}
                      textInputProps={{ onFocus: this._onFocusInputA.bind(this) }}
                      onPress={data => {
                        this.setState({origin: data});
                        this.props.onChangeOrigin(data);
                      }}
                      getDefaultValue={() => {}}
                      query={queryParameters}
                      styles={inputStyles}
                      renderRow={ data => <GoogleAutocompleteItem data={data} recent={recentSearches} /> }
                      currentLocation={false} />
                    <TouchableOpacity
                      style={[styles.inputIconContainer, focusRef == 'inputA' && styles.inputIconActive]}
                      activeOpacity={0.7}
                      onPress={this._onFocusInputA.bind(this)}>
                      <Text style={styles.inputIconText}>A</Text>
                    </TouchableOpacity>
                </View>
                <View>
                    <GooglePlacesAutocomplete
                      ref="inputB"
                      placeholder='Enter destination place'
                      minLength={2}
                      autoFocus={false}
                      fetchDetails={true}
                      textInputProps={{ onFocus: this._onFocusInputB.bind(this) }}
                      onPress={data => {
                        this.setState({destination: data});
                        this.props.onChangeDestination(data);
                      }}
                      getDefaultValue={() => {}}
                      query={queryParameters}
                      styles={inputStyles}
                      renderRow={ data => <GoogleAutocompleteItem data={data} recent={recentSearches} /> }
                      currentLocation={false} />
                    <TouchableOpacity
                      style={[styles.inputIconContainer, focusRef == 'inputB' && styles.inputIconActive]}
                      activeOpacity={0.7}
                      onPress={this._onFocusInputB.bind(this)}>
                        <Text style={styles.inputIconText}>B</Text>
                    </TouchableOpacity >
                </View>
            </View>
            <TouchableOpacity
              style={{position: 'absolute', top: 40, right: 0, zIndex: 99}}
              activeOpacity={0.7}
              onPress={this.props.onSwap}>
                <Image source={reverseIcon} style={styles.reverseIcon}/>
            </TouchableOpacity>
          </View>
        </View>
      );
    }
    _onFocusInputA() {
      this.props.onFocus('inputA');
      this.refs.inputB.triggerBlur();
      this.setState({focusRef: 'inputA'});
    }
    _onFocusInputB() {
      this.props.onFocus('inputB');
      this.refs.inputA.triggerBlur();
      this.setState({focusRef: 'inputB'});
    }
    _onBlur() {
      this.refs.inputB.triggerBlur();
      this.refs.inputA.triggerBlur();
      this.props.onBlur();
      this.setState({focusRef: ''});
    }
    _updateFields(props, state) {
      const { origin, destination } = props;
      const cOrigin = state.origin;
      const cDestination = state.destination;

      if(!_.isEqual(origin, cOrigin)) {
        this.refs.inputA.changeText(origin);
        this.setState({origin});
        if(destination) this._onBlur();
      }

      if(!_.isEqual(destination, cDestination)) {
        this.refs.inputB.changeText(destination);
        this.setState({destination});
        if(origin) this._onBlur();
      }
    }
}

const styles = StyleSheet.create({
    inputWrapper: {
        flex: 1,
        position: 'relative',
        marginTop: 7,
        marginRight: 30
    },
    inputContainer: {
        borderRadius: 4,
        backgroundColor: '#fff',
        height: 42,
        alignItems: 'center',
        marginBottom: 8
    },
    input: {
        flex: 1,
        marginLeft: 35,
        alignSelf: 'stretch',
        fontFamily: 'AvenirNextLTPro-Demi',
        fontSize: 14,
        color: 'rgba(53, 67, 88, 0.702)',
        marginHorizontal: 2
    },
    reverseIcon: {
        marginLeft: 8,
        marginRight: 8,
        width: 17,
        height: 24
    },
    inputIconContainer: {
        position: 'absolute',
        top: 10,
        left: 12,
        width: 25,
        height: 25,
        borderRadius: 25,
        alignItems: 'center',
        justifyContent: 'center',
    },
    inputIconText: {
        fontFamily: 'AvenirNextLTPro-Bold',
        color: 'rgb(53, 67, 88)',
        fontSize: 13,
        lineHeight: 16,
    },
    inputIconActive: {
        backgroundColor: 'rgb(249, 220, 92)'
    },
    listView: {
        borderRadius: 4,
        backgroundColor: '#fff',
        elevation : 3,
        marginBottom: 300
    },
    separator: {
        marginLeft: 45,
        height: 1,
        borderBottomColor: 'rgba(65, 77, 94, 0.09)'
    }
});
