
import React, { Component } from 'react';
import { View, StyleSheet, Image, Text, Switch, Dimensions } from 'react-native';
import _ from 'lodash';
// import polyline from 'polyline';
import MapView from 'react-native-maps';
import Marker from '../Marker';
import DirectionsView from './DirectionsView';
import { midpoint } from '../../utils/utils';

export default class Map extends Component {
    constructor(props) {
      super(props);

      const { width, height } = Dimensions.get('window');

      this.state = {
        region: null,
        screen: { width, height },
        ASPECT_RATIO: width/height,
        LATITUDE_DELTA: 0.0041,
        LONGITUDE_DELTA: 0.0041 * width/height,
      }
    }
    componentWillMount() {
      if(this.props.chosenDirection) {
        const region = {
          latitude: this.props.chosenDirection.data.start_location.lat,
          longitude: this.props.chosenDirection.data.start_location.lng,
          latitudeDelta: this.state.LATITUDE_DELTA,
          longitudeDelta: this.state.LONGITUDE_DELTA
        };
        this.onRegionChange(region);
      }
    }
    componentWillReceiveProps(nextProps) {
      const { chosenDirection, isActiveMockLocation, mockLocation } = nextProps;

      if(this.props.chosenDirection != chosenDirection && chosenDirection) {
        this.setFocusMapOnDirection(chosenDirection);
      }
      
      if(this.props.isActiveMockLocation != isActiveMockLocation) {
        if(isActiveMockLocation) {
          // this.setFocusMapOnStart();
        } else {
          this.setFocusMapOnDirection(chosenDirection);
        }
      }

      // if(mockLocation.coordinate && this.props.mockLocation.coordinate != mockLocation.coordinate) {
      //   this.setRegion(mockLocation.coordinate);
      // }

    }
    shouldComponentUpdate(nextProps, nextState) {
        return !_.isEqual(nextProps, this.props) || !_.isEqual(nextState, this.state);
    }
    onRegionChange = region => {
        this.setState({ region });
    }
    render() {
      const { direction, directions, chosenDirection, isActiveMockLocation } = this.props;
      console.log('re-render Map')
      return <MapView
                style={styles.map}
                region={this.state.region}
                onRegionChange={this.onRegionChange}>
                { this.renderDirections() }
                { this.renderPassedSteps() }
                { this.renderMockLocationMarker() }
                { this.renderOriginalLocationMarker() }
                { this.renderDirectionMarkers() }
              </MapView>
    }
    renderPassedSteps = () => {
      const { passedSteps } = this.props;
      
      if(passedSteps == null) return null;
      
      return (
        <MapView.Polyline
          key="passedSteps"
          style={styles.passedSteps}
          strokeWidth={7}
          coordinates={passedSteps}
          strokeColor={'rgb(249, 220, 92)'}/>
      );
    }
    renderMockLocationMarker = () => {
       const { mockLocation, isPausedMockDirection } = this.props;

       if(mockLocation.coordinate) {
         if(isPausedMockDirection) {
            return <Marker
                    key='mockLocation'
                    style={{zIndex: 100}}
                    type='mockLocationHold'
                    coordinate={mockLocation.coordinate}
                  />
         } else {
           return <Marker
                    key='mockLocation'
                    style={{zIndex: 100}}
                    type='mockLocation'
                    coordinate={mockLocation.coordinate}
                  />
         }
      } else {
        return null;
      }
    } 
    renderOriginalLocationMarker = () => {
      const { originalLocation } = this.props;

      if(originalLocation) {
        return <Marker
            key='actualLocation'
            style={{zIndex: 3}}
            type='default'
            coordinate={originalLocation}
        />
      } else {
        return null;
      }
    }
    renderDirectionMarkers() {
      const { chosenDirection } = this.props;

      if(chosenDirection) {
        const originCoordinate = {
          latitude: chosenDirection.data.start_location.lat,
          longitude: chosenDirection.data.start_location.lng
        };
        const destinationCoordinate = {
          latitude: chosenDirection.data.end_location.lat,
          longitude: chosenDirection.data.end_location.lng
        };

        return [
          <Marker
            key='1'
            style={{zIndex: 3}}
            type='start'
            anchor={{x:0.5, y:1}}
            coordinate={originCoordinate}/>,
          <Marker
            key='2'
            style={{zIndex: 3}}
            type='end'
            anchor={{x:0.5, y:1}}
            coordinate={destinationCoordinate}/>
        ];
      } else {
        return null;
      }
    }
    renderDirections = () => {
      const { directions, chosenDirection, isActiveMockLocation } = this.props;
      if(directions && directions.length) {
        return directions.map((d, i) => <DirectionsView 
                                          key={i}      
                                          direction={d} 
                                          chosenDirection={chosenDirection} 
                                          isActiveMockLocation={isActiveMockLocation}/>
                          );
      } else {
        return null;
      }
    }
    setFocusMapOnDirection = direction => {
        const { start_location, end_location } = direction.data;

        const LATITUDE_DELTA = Math.abs(start_location.lat-end_location.lat)*3.5;
        const LONGITUDE_DELTA = Math.abs(start_location.lng-end_location.lng)*2;

        const coordinate = midpoint(start_location.lat, start_location.lng, end_location.lat, end_location.lng);
            
        const region = {
          latitude: coordinate.latitude,
          longitude: coordinate.longitude,
          latitudeDelta: LATITUDE_DELTA,
          longitudeDelta: LONGITUDE_DELTA
        };

        this.onRegionChange(region);
    }
    setFocusMapOnStart = () => {
      const { selected } = this.props;
      
      if(selected == null) return;  
      
      const region = {
        latitude: selected.data.start_location.lat,
        longitude: selected.data.start_location.lng,
        latitudeDelta: this.state.LATITUDE_DELTA,
        longitudeDelta: this.state.LONGITUDE_DELTA
      };

      this.onRegionChange(region);

    }
    setRegion = coordinate => {      
      const region = {
        latitude: coordinate.latitude,
        longitude: coordinate.longitude,
        latitudeDelta: this.state.LATITUDE_DELTA,
        longitudeDelta: this.state.LONGITUDE_DELTA
      };

      this.onRegionChange(region);
    }
}

const styles = StyleSheet.create({
  map: {
      ...StyleSheet.absoluteFillObject
  },
  passedSteps: {
    zIndex: 3,
  }
});
