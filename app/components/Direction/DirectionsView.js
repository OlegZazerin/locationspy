
import React, { Component } from 'react';
import _ from 'lodash';
import polyline from 'polyline';
import MapView from 'react-native-maps';

export default class DirectionView extends Component {
     constructor(props) {
      super(props);

      this.state = {
        coordinates: props.direction ? this.getCoordinates(props.direction.data) : null
      }
    } 
    shouldComponentUpdate(nextProps, nextState) {
        return !_.isEqual(nextProps, this.props) || !_.isEqual(nextState, this.state);
    }
    render() {
        const { direction, chosenDirection, isActiveMockLocation } = this.props;
        console.log('re-render DirectionView');
        let style = {zIndex: 1} , strokeColor = "#7b8188";

        if(isActiveMockLocation) {
            strokeColor = "transparent";
        }

        if(chosenDirection && chosenDirection.type == direction.type) {
            style = {zIndex: 2};
            strokeColor = '#506e86';
        }
        // return null;
        return <MapView.Polyline
                style={style}
                strokeWidth={5}
                coordinates={this.state.coordinates}
                strokeColor={strokeColor}/>
    }
    getCoordinates = data => {
        console.log(data);
      return data.steps.reduce((c, s) => {
        const points = polyline.decode(s.polyline.points).map(e => ({ latitude: e[0], longitude: e[1] }));
        return [ ...c, ...points ];
      }, []);
    }
}