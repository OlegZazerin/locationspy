
import React, { Component } from 'react';
import { Animated, View, StyleSheet, Image, Text, TouchableNativeFeedback  } from 'react-native';
import Tabs from 'react-native-tabs';
import _ from 'lodash';

var ScrollableTabView = require('react-native-scrollable-tab-view');

const bicyclingImg = require('../img/bicycling.png');
const bicyclingActiveImg = require('../img/bicycling_active.png');

const drivingImg = require('../img/driving.png');
const drivingActiveImg = require('../img/driving_active.png');

const walkingImg = require('../img/walking.png');
const walkingActiveImg = require('../img/walking_active.png');

export default class DirectionsBar extends Component {
    shouldComponentUpdate(nextProps, nextState) {
        return !_.isEqual(nextProps, this.props) || !_.isEqual(nextState, this.state);
    }
    render() {
      const { hide, chosenDirection, directions } = this.props;
      console.log('re-render DirectionsBar', !hide);
      
      if(hide) return null;
      
      return (
          <ScrollableTabView
            style={styles.container}
            onChangeTab={this.onChange}
            renderTabBar={() => <TabBar />}>
            { directions.map((d, i) => <View key={i} tabLabel={{ icon: d.type, label: d.data.duration.text}} value={d} />) }
          </ScrollableTabView>
      );

    }
    onChange = value => {
      const { onChange } = this.props;
      if(typeof onChange === 'function') {
        try {
          onChange(value.ref.props.value);
        } catch(error) {
          console.log(error.toString());
        }
      }
    }
}

class TabBar extends Component {
  constructor(props) {
    super(props);

    this.state = {
      activeTextColor: 'navy',
      inactiveTextColor: 'black',
      backgroundColor: null,
    };
  }

  renderTab(tab, page, isTabActive, onPressHandler) {
    return <TouchableNativeFeedback
      key={tab.label}
      style={{flex: 1}}
      onPress={() => onPressHandler(page)}>
      <View style={styles.tabWrapper}>
        { this._getIcon(tab.icon, isTabActive) }
        <Text style={[styles.label, isTabActive && {color: '#f9dc5c'}]}>{tab.label}</Text>
      </View>
    </TouchableNativeFeedback >;
  }

  render() {
    const containerWidth = this.props.containerWidth;
    const numberOfTabs = this.props.tabs.length;
    const underlineWidth = containerWidth / numberOfTabs;
    const tabUnderlineStyle = {
      position: 'absolute',
      width: underlineWidth,
      height: 4,
      backgroundColor: '#f9dc5c',
      bottom: 0,
    };

    const left = this.props.scrollValue.interpolate({
      inputRange: [0, 1], outputRange: [0,  underlineWidth],
    });

    return (
      <View style={[styles.tabs, {backgroundColor: this.props.backgroundColor}, this.props.style, ]}>
        {this.props.tabs.map((tab, page) => {
          const isTabActive = this.props.activeTab === page;
          return this.renderTab(tab, page, isTabActive, this.props.goToPage);
        })}
        <Animated.View style={[tabUnderlineStyle, { left }, this.props.underlineStyle ]} />
      </View>
    );
  }
  _getIcon(name, active) {
    switch (name) {
      case 'bicycling':
        return <Image source={ active ? bicyclingActiveImg : bicyclingImg} style={styles.bicycling}/>
      case 'driving':
        return <Image source={ active ? drivingActiveImg : drivingImg} style={styles.driving}/>
      case 'walking':
        return <Image source={ active ? walkingActiveImg : walkingImg} style={styles.walking}/>
    }
  }
}

const styles = StyleSheet.create({
    container: {
      height: 50,
      position: 'absolute',
      top: 110,
      left:0,
      right:0,
      backgroundColor:'rgba(47, 60, 79, 0.95)'
    },
    active: {
      color:'#f9dc5c'
    },
    tabs: {
      height: 50,
      flexDirection: 'row',
      justifyContent: 'space-around',
      borderWidth: 1,
      borderTopWidth: 0,
      borderLeftWidth: 0,
      borderRightWidth: 0,
      borderColor: '#ccc',
    },
    tabWrapper: {
      flex: 1,
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'center'
    },
    tabWrapperActive: {
      borderBottomWidth: 4,
      borderBottomColor: '#f9dc5c'
    },
    label: {
      fontSize: 13,
      fontFamily: 'AvenirNextLTPro-Demi',
      marginLeft: 12,
      color: '#b0b5bc'
    },
    bicycling: {
      width: 30,
      height: 20.5
    },
    driving: {
      width: 20,
      height: 17
    },
    walking: {
      width: 12,
      height: 20
    }
});
