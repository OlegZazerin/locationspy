
import React, { Component } from 'react';
import { View, StyleSheet, Image, Text, Switch } from 'react-native';
import _ from 'lodash';
import * as utils from '../../utils/utils';
import Button from '../UI/Button';

export default class Manager extends Component {
    constructor(props) {
      super(props);

      this.state = {
        autoDeactivation: props.direction.autoDeactivation || false
      }
    }
    shouldComponentUpdate(nextProps, nextState) {
        return !_.isEqual(nextProps, this.props) || !_.isEqual(nextState, this.state);
    }
    render() {
      const { direction, hide } = this.props;
      console.log('re-render Manager')
      if(!direction.direction || hide) return null;

      if(direction.isActiveMockLocation) {
        let addressOrigin = '', localityOrigin = '', addressDestination = '', localityDestination ='', minutes = '', seconds = '';
        let passedDistance = (direction.distance/1000).toFixed(1);

        if(direction.origin) {
          addressOrigin = utils.getFormatedAddress(direction.origin.address_components);
          localityOrigin = utils.getFormatedLocality(direction.origin.address_components);
        }
        
        if(direction.destination) {
          addressDestination = utils.getFormatedAddress(direction.destination.address_components);
          localityDestination = utils.getFormatedLocality(direction.destination.address_components);
        }

        if(direction.timeLeft) {
            minutes = (direction.timeLeft/60).toFixed(0).toString().replace(/^(\d)$/,'0$1');
            seconds = (direction.timeLeft%60).toFixed(0).toString().replace(/^(\d)$/,'0$1');
        }
        
        return (
              <View style={styles.container}>
                <View style={{justifyContent: 'center', alignItems: 'center', marginVertical: 10}}>
        <Text style={styles.activeDescription}>{passedDistance} / {direction.direction.data.distance.text} {"\u00B7"} {minutes}:{seconds} min</Text>
                </View>
                <View style={styles.separator} />
                <View style={{flexDirection: 'row', alignItems: 'center', marginLeft: 13, marginTop: 10}}>
                    <Text style={styles.directionIcon}>A</Text>
                    <View style={{marginLeft: 13, paddingBottom: 10, borderBottomWidth: 1, borderBottomColor: 'rgba(255, 255, 255, 0.04)'}}>
                      <Text style={{color: '#fff', fontWeight: 'bold'}}>{addressOrigin}</Text>
                      <Text style={{color: 'rgba(255, 255, 255, 0.4)'}}>{localityOrigin}</Text>
                    </View>
                </View>
                <View style={{flexDirection: 'row', alignItems: 'center', marginLeft: 13, marginTop: 10}}>
                    <Text style={styles.directionIcon}>B</Text>
                    <View style={{marginLeft: 13, paddingBottom: 10, borderBottomWidth: 1, borderBottomColor: 'rgba(255, 255, 255, 0.04)', flex: 1}}>
                      <Text style={{color: '#fff', fontWeight: 'bold'}}>{addressDestination}</Text>
                      <Text style={{color: 'rgba(255, 255, 255, 0.4)'}}>{localityDestination}</Text>
                    </View>
                </View>
                <View style={styles.actions}>
                  <Button
                    title="DEACTIVATE"
                    styleWrapper={{borderBottomLeftRadius: 4, borderBottomRightRadius: 4}}
                    backgroundColor={'rgb(244, 96, 54)'}
                    color={'#fff'}
                    onPress={() => { this.props.onChangeActiveState(false) }} />
                </View>
              </View>
        );
      } else {
        return (
              <View style={styles.container}>
                <View style={[styles.item, {justifyContent: 'center'}]}>
                  { direction.directions.map((e, i) => <View key={i} style={[styles.icon, e.type === direction.direction.type && styles.iconActive]}/>) }
                </View>
                <View style={styles.separator} />
                <View style={[styles.item, {justifyContent: 'center', alignItems: 'center'}]}>
                  <Text style={styles.text}>Auto-deactivation on arrival</Text>
                  <Switch
                    style={styles.switch}
                    value={this.state.autoDeactivation}
                    onValueChange={this.onChangeAutoDeactivation}/>
                </View>
                <View style={styles.separator} />
                <View style={styles.item}>
                  <View style={[styles.itemCol, {flex: 1, alignItems: 'center'}]}>
                    <Text style={styles.time}>{direction.direction.data.duration.text}</Text>
                  </View>
                  <View style={{flex: 1, alignItems: 'center'}}>
                    <Text style={styles.distance}>{direction.direction.data.distance.text}</Text>
                  </View>
                </View>
                <View style={styles.actions}>
                  <Button
                    title="ACTIVATE LOCATIONSPY"
                    styleWrapper={{borderBottomLeftRadius: 4, borderBottomRightRadius: 4}}
                    onPress={() => { this.props.onChangeActiveState(true) }} />
                </View>
              </View>
        );
      }
    }
    onChangeAutoDeactivation = value => {
      const { onChangeAutoDeactivation } = this.props;

      this.setState({autoDeactivation: value});

      if(typeof onChangeAutoDeactivation === 'function') {
        onChangeAutoDeactivation(value);
      }
    }
}

const styles = StyleSheet.create({
    container: {
        width: 250,
        marginBottom: 10,
        borderWidth: 0  ,
        borderRadius: 5,
        backgroundColor: 'rgba(47, 60, 79, 0.95)',
        alignSelf: 'center',
        zIndex: 10
    },
    icon: {
      width: 10,
      height: 10,
      borderRadius: 10,
      backgroundColor: 'rgba(255, 255, 255, 0.2)',
      marginHorizontal: 2
    },
    iconActive: {
      backgroundColor: 'rgb(249, 220, 92)'
    },
    separator: {
      height: 2,
      backgroundColor: 'rgba(255, 255, 255, 0.04)'
    },
    verticalSeparator:{
      flex: 1,
      width: 2,
      backgroundColor: 'rgba(255, 255, 255, 0.04)'
    },
    item: {
      flexDirection: 'row',
      flexWrap: 'wrap',
      justifyContent: 'space-around',
      alignItems: 'stretch',
      paddingVertical: 13
    },
    itemCol: {
      borderRightWidth: 1,
      borderRightColor: 'rgba(255, 255, 255, 0.04)'
    },
    time: {
        fontFamily: 'AvenirNextLTPro-Bold',
        fontSize: 24,
        color: '#fff',
        lineHeight: 26,
        textAlign: 'center'
    },
    distance: {
        fontFamily: 'AvenirNextLTPro-Bold',
        fontSize: 24,
        color: '#fff',
        lineHeight: 26,
        textAlign: 'center'
    },
    switch: {
      marginLeft: 10
    },
    activeDescription: {
      color: 'rgba(255, 255, 255, 0.4)',
      fontSize: 14,
    },
    directionIcon: {
      color: 'rgb(249, 220, 92)',
      fontSize: 16,
      fontFamily: 'AvenirNextLTPro-Bold',
      paddingBottom: 10
    },
    text: {
        fontFamily: 'AvenirNextLTPro-Demi',
        fontSize: 12,
        color: '#fff',
        lineHeight: 14
    }
});
