import React, { Component } from 'react';
import { View, StyleSheet, Image, TouchableHighlight } from 'react-native';

const backToIconSrc = require('./img/back_to.png');

export default class BackToOriginalLocationBtn extends Component {
    render() {
        const { show, onPress } = this.props;

        if(show) {
            return (
                <TouchableHighlight
                    underlayColor="#fff"
                    style={styles.container}
                    onPress={onPress}>
                  <Image source={backToIconSrc} style={styles.icon} />
                </TouchableHighlight>
            );
        } else {
          return null;
        }
    }
}

const styles = StyleSheet.create({
    container: {
        position: 'absolute',
        right: 0,
        bottom: 20,
        width: 40,
        height: 40,
        zIndex: 1,
        borderBottomLeftRadius: 4,
        borderTopLeftRadius: 4,
        backgroundColor: '#fff',
        justifyContent: 'center',
        alignItems: 'center',
        elevation: 1
    },
    icon: {
        width: 15,
        height: 15
    }
});
