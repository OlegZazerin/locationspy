
import React, { Component } from 'react';
import { StatusBar, View, StyleSheet, Text, PixelRatio } from 'react-native';

export default class TabIcon extends Component {
    render(){
        const { selected } = this.props;
        return <Text style={[styles.tabIconText, selected && styles.tabIconTextActive]}>{this.props.title}</Text>
    }
}

const styles = StyleSheet.create({
    tabIconText: {
        fontFamily: 'AvenirNextLTPro-Bold',
        fontSize: 4 * PixelRatio.getFontScale(),
        color: '#5e6b7a'
    },
    tabIconTextActive: {
        color: '#3a4c61'
    },
});
