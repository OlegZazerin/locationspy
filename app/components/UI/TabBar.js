import React, { Component, PropTypes } from 'react';
import {
    Animated,
    StyleSheet,
    Text,
    View,
} from 'react-native';
import { TabBar } from 'react-native-tab-view';

const styles = StyleSheet.create({
    tabBarWrapper:{
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        backgroundColor: 'rgba(47, 60, 79, 0.95)',
        paddingHorizontal: 24,
        paddingVertical: 16,
        zIndex: 1
    },
    tabBarContainer: {
        position: 'relative',
        backgroundColor: 'rgb(80, 110, 134)',
        borderRadius: 42,
        height: 42,
    },
    tabLabel: {
        fontFamily: 'AvenirNextLTPro-Bold',
        fontSize: 16,
        lineHeight: 24,
        color: '#fff',
        borderRadius: 42
    },
    activeLabel:{
        color: '#384556'
    },
    indicator: {
        backgroundColor: '#f9dc5c',
        position: 'absolute',
        top: 2,
        left: 2,
        height: 38,
        borderRadius: 42,
    },
    holdBlock: {
        position: 'absolute',
        top:0,
        left:0,
        right:0,
        bottom: 0,
        backgroundColor: 'transparent'
    },
    holdTabBar: {
        backgroundColor: 'rgb(59, 78, 99)'
    },
    holdLabel: {
        color: 'rgb(94, 107, 122)'
    }
});

export default class CustomTabBar extends Component {

    _renderLabel = ({route, focused}) => (
        route.title ? <Text style={[ styles.tabLabel, this.props.hold && styles.holdLabel, focused && styles.activeLabel ]}>{route.title}</Text> : null
    );

    _renderIndicator = props => {
        let { width, position } = props;

        const translateX = Animated.multiply(position, new Animated.Value(width));

        return (
            <Animated.View
                style={[ styles.indicator, { width, transform: [ { translateX } ] }, this.props.indicatorStyle ]}
            />
        );
    };

    render() {
        const props = { ...this.props, layout: { ...this.props.layout, width: this.props.layout.width - 52} }

        return (
            <View style={styles.tabBarWrapper}>
              <View style={[styles.tabBarContainer, props.hold && styles.holdTabBar]}>
                <TabBar
                    style={{backgroundColor: 'transparent'}}
                    {...props}
                    scrollEnabled={false}
                    activeOpacity={1}
                    renderLabel={this._renderLabel}
                    renderIndicator={this._renderIndicator}
                />
              </View>
              { props.hold ? <View style={styles.holdBlock}/> : null }
            </View>
        );
    }
}
