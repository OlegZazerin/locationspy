import React, {Component} from 'react';
import {
    StyleSheet,
    Animated,
    Dimensions,
    View
} from 'react-native';

import _ from 'lodash';

class SlideUpContainer extends Component {
  constructor(props) {
     super(props);

     var {height, width} = Dimensions.get('window');

     this.state = {
       visible: false,
       modalY: new Animated.Value(height),
       layout: { width, height }
     };
  }
  componentWillMount() {
    if(this.props.visible) {
      this.open();
    }
  }
  componentWillReceiveProps(nextProps) {
    if(this.state.visible != nextProps.visible) {
      if(nextProps.visible) {
        this.open();
      } else {
        this.close();
      }
    }
  }
  shouldComponentUpdate(nextProps, nextState) {
      return this.state.visible != nextProps.visible;
  }
  open = () => {
    console.log('open');
    Animated.timing(this.state.modalY, {
        duration: 500,
        toValue: 0
     }).start();
     this.setState({visible: true});
  }
  close = () => {
    console.log('close');
    Animated.timing(this.state.modalY, {
        duration: 500,
        toValue: this.state.layout.height
     }).start();
     this.setState({visible: false});
  }
  _onLayout = event => this.appLayout(event.nativeEvent.layout);
  appLayout = event => {
    const { width, height } = event;
    this.setState({layout: { width, height }});
  }
  render() {
    console.log('re-render slide up');
    return (
      <Animated.View
        style={[styles.wrapper, { transform: [{translateY: this.state.modalY}] }]}
        onLayout={this._onLayout}>
          {this.props.children}
      </Animated.View>
    );
  }
}

export default SlideUpContainer;

const styles = StyleSheet.create({
    wrapper: {
        position: 'absolute',
        top:0,
        right: 0,
        left:0,
        bottom: 0,
        backgroundColor: 'transparent'
    }
});
