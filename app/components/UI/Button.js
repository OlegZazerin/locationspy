
import React, { Component } from 'react';
import {
    ColorPropType,
    Platform,
    PixelRatio,
    StyleSheet,
    Text,
    View,
    TouchableNativeFeedback,
    TouchableOpacity
} from 'react-native';

export default class Button extends Component {

    render() {
        const {
            color,
            onPress,
            styleWrapper,
            styleText,
            title,
            disabled,
            backgroundColor
        } = this.props;

        return (
            <TouchableNativeFeedback
                disabled={disabled}
                onPress={() => {
                    if(typeof onPress === 'function') {
                        onPress();
                    }
                }}>
              <View style={[styles.button, styleWrapper, disabled && styles.buttonDisabled, backgroundColor && {backgroundColor}]}>
                <View>
                  <Text style={[styles.text, disabled && styles.buttonDtextDisabledisabled, color && {color}]}>{title.toUpperCase()}</Text>
                </View>
              </View>
            </TouchableNativeFeedback>
        );
    }
}

const styles = StyleSheet.create({
    button: Platform.select({
        ios: {},
        android: {
            elevation: 4,
            justifyContent: 'center',
            backgroundColor: "#efd45a",
            height: 43
        },
    }),
    text: Platform.select({
        ios: {
            color: '#2196F3',
            textAlign: 'center',
            padding: 8,
            fontSize: 18,
        },
        android: {
            textAlign: 'center',
            fontFamily: 'AvenirNextLTPro-Bold',
            fontSize: 14,
            color: '#384556'
        },
    }),
    buttonDisabled: Platform.select({
        ios: {},
        android: {
            elevation: 0,
            backgroundColor: '#dfdfdf',
        }
    }),
    textDisabled: Platform.select({
        ios: {
            color: '#cdcdcd',
        },
        android: {
            color: '#a1a1a1',
        }
    }),
});
