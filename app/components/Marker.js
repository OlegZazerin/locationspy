
import React, { Component } from 'react';
import { View, StyleSheet, Image } from 'react-native';

import MapView from 'react-native-maps';

import _ from 'lodash';

const movedMarkerImg = require('./img/moved_marker.png');
const defaultMarker = require('./img/default_marker.png');
const defaultMarkerDraggable = require('./img/default_marker_draggable.png');
const fixedMarkerImg = require('./img/fixed_marker.png');
const startRouteMarkerImg = require('./img/start_route_marker.png');
const endRouteMarkerImg = require('./img/end_route_marker.png');
const currentLocation = require('./img/current_location.png');
const currentLocationHold = require('./img/current_location_hold.png');

export default class Marker extends Component {
    constructor(props) {
        super(props);

        this.state = {
          ladingIconEnd: false
        };
    }
    shouldComponentUpdate(nextProps, nextState) {
        if(this.props.type !== nextProps.type) {
            return true;
        } else if(this.props.coordinate !== nextProps.coordinate) {
            return true;
        } else if(this.props.draggable !== nextProps.draggable) {
            return true;
        } else if(nextState != this.state) {
          return true;
        }
        return false;
    }
    render() {
      return <MapView.Marker
                  {...this.props}
                  key={this.state.ladingIconEnd ? '1' : '2'}
                  anchor={this.props.anchor || {x:0.5, y:0.5}}>
                  <MarkerIcon
                      ref="markerIcon"
                      onLoadIconSuccess={() => {this.setState({ladingIconEnd: true}) }}
                      type={this.props.type}
                  />
              </MapView.Marker>
    }
}

class MarkerIcon extends Component {
    constructor(props){
        super(props);

        this.state = {
            type: props.type || 'default',
        }
    }
    componentWillReceiveProps(nextProps) {
      this.setState({type: nextProps.type});
    }
    render() {
        const { type } = this.state;

        switch(type) {
            case 'move':
                return <Image
                            key="move"
                            source={movedMarkerImg}
                            onLoadEnd={this.props.onLoadIconSuccess}
                            style={{zIndex: 1, width: 20, height: 45}}
                        />;
            case 'draggable':
                return <Image
                            key="draggable"
                            source={defaultMarkerDraggable}
                            onLoadEnd={this.props.onLoadIconSuccess}
                            style={{zIndex: 1000, width: 30, height: 30}}
                        />;
            case 'fixed':
                return <Image
                            key="fixed"
                            source={fixedMarkerImg}
                            onLoadEnd={this.props.onLoadIconSuccess}
                            style={{zIndex: 1, width: 35, height: 33}}
                        />;
            case 'start':
                return <Image
                            key="start"
                            source={startRouteMarkerImg}
                            onLoadEnd={this.props.onLoadIconSuccess}
                            style={{zIndex: 1, width: 29, height: 45}}
                        />;
            case 'end':
                return <Image
                            key="end"
                            source={endRouteMarkerImg}
                            onLoadEnd={this.props.onLoadIconSuccess}
                            style={{zIndex: 1, width: 29, height: 45}}
                        />;
            case 'mockLocationHold':
                return <Image
                            key="mockLocationHold"
                            source={currentLocationHold}
                            onLoadEnd={this.props.onLoadIconSuccess}
                            style={{zIndex: 1, width: 20, height: 20}}
                        />;            
            case 'mockLocation':
                return <Image
                            key="mockLocation"
                            source={currentLocation}
                            onLoadEnd={this.props.onLoadIconSuccess}
                            style={{zIndex: 1, width: 20, height: 20}}
                        />;                        
            default:
                return <Image
                            key="default"
                            source={defaultMarker}
                            onLoadEnd={this.props.onLoadIconSuccess}
                            style={{zIndex: 1, opacity: 1, width: 30, height: 30}}
                        />;
        }
    }
}
