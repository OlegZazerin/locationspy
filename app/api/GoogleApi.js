import Qs from 'qs';
import _ from 'lodash';

export default class GoogleAPI {
    constructor(key) {
        this.url = 'https://maps.googleapis.com/maps/api';
        this.key = 'AIzaSyD7f1KjELWTkxKf0z44euJVMZjKkSuANGM';
    }
  /**.
   * @public
   */
   geocodeLocation(coordinate) {
     return fetch(`${this.url}/geocode/json?`+Qs.stringify({
               latlng:`${coordinate.latitude||coordinate.lat},${coordinate.longitude||coordinate.lng}`,
               key: this.key,
               language: 'en'
             })
           )
           .then(response => response.json())
           .catch(e => console.log(e.toString()));
   }
   /**.
    * @public
    */
  geocodeAddress(address) {
    return fetch(`${this.url}/geocode/json`, {
        method: 'POST',
        body: JSON.stringify({
            key: this.key,
            address
        })
    })
    .then(r =>console.log(r))
    .catch(e => console.log(e.toString()));
  }
  /**.
   * @public
   */
  getDirections(origin, destination, mode) {
    return fetch(`${this.url}/directions/json?`+Qs.stringify({
          key: this.key,
          mode,
          origin: 'place_id:' + origin,
          destination: 'place_id:' + destination
        })
      )
      .then(response => response.json())
      .catch(e => console.log(e.toString()));
  }
}
