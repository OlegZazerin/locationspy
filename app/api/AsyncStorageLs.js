import { AsyncStorage } from 'react-native';
import _ from 'lodash';

export default class AsyncStorageLs {
  /**.
   * @public
   */
   async saveRecentPaths(value) {
     try {
       await AsyncStorage.setItem('@Locationspy:RecentPaths', value);
     } catch (error) {
       console.log(error.toString());
     }
   }
  /**.
   * @public
   */
   async loadingRecentPaths() {
     try {
       const value = await AsyncStorage.getItem('@Locationspy:RecentPaths');
       if (value !== null){
         return value;
       }
     } catch (error) {
       console.log(error.toString());
     }
     return [];
   }
   /**.
    * @public
    */
    async saveRecentSearches(value) {
      try {
        await AsyncStorage.setItem('@Locationspy:RecentSearches', value);
      } catch (error) {
        console.log(error.toString());
      }
    }
  /**.
   * @public
   */
   async loadingRecentSearches() {
     try {
       const value = await AsyncStorage.getItem('@Locationspy:RecentSearches');
       if (value !== null){
         return value;
       }
     } catch (error) {
       console.log(error.toString());
     }
     return [];
   }
}
