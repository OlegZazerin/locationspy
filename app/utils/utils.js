import polyline from 'polyline';

export function getFormatedAddress(address_components) {
    return `${extractFromAdress(address_components, "street_number")} ${extractFromAdress(address_components, "route")}`;
}

export function getFormatedLocality(address_components) {
    let locality = extractFromAdress(address_components, "locality");
    if(!locality) {
        locality = extractFromAdress(address_components, "administrative_area_level_1");
    }
    return `${locality} ${extractFromAdress(address_components, "country")}`
}

export function extractFromAdress(components, type){
    for (var i=0; i<components.length; i++)
        for (var j=0; j<components[i].types.length; j++)
            if (components[i].types[j]==type) return components[i].long_name;
    return "";
}

export function getDistanceFromLatLonInMeters(lat1,lon1,lat2,lon2) {
  var R = 6371; // Radius of the earth in km
  var dLat = deg2rad(lat2-lat1);  // deg2rad below
  var dLon = deg2rad(lon2-lon1); 
  var a = 
    Math.sin(dLat/2) * Math.sin(dLat/2) +
    Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * 
    Math.sin(dLon/2) * Math.sin(dLon/2)
    ; 
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
  var d = R * c; // Distance in km
  return d*1000;
}

function deg2rad(deg) {
  return deg * (Math.PI/180)
}

export function midpoint(lat1, long1, lat2, long2) {
     return { latitude: lat1 + (lat2 - lat1)*0.5, longitude: long1 + (long2 - long1)*0.5 };
}

export function getDataStep(direction, index) {
  
  if(direction == null || direction.data.steps.length == 0 || !direction.data.steps[index]) return null;

  const step = direction.data.steps[index]
  const oneStepDistance = Math.round(step.distance.value/step.duration.value);
  
  return {
    index,
    start_location: step.start_location,
    end_location: step.end_location,
    distance: step.distance.value,
    duration: step.duration.value,
    oneStepDistance,
    points: polyline.decode(step.polyline.points).map((point, i, points) => {
      if(i) {
        const previousPoint = points[i-1];
        const distance = Math.round(getDistanceFromLatLonInMeters(previousPoint[0], previousPoint[1], point[0],point[1]));
        const steps = Math.round(distance/oneStepDistance);
        const diffStep = {
          latitude: (point[0] - previousPoint[0])/steps,
          longitude: (point[1] - previousPoint[1])/steps
        }

        let tempPoint = {
          latitude: previousPoint[0],
          longitude: previousPoint[1],
        }

        return [...Array(steps)].map(e => {
          const newPoint = {
            latitude: tempPoint.latitude + diffStep.latitude,
            longitude: tempPoint.longitude + diffStep.longitude
          };
          tempPoint = newPoint;
          return newPoint;
        });
      } else {
        return  [{ latitude: point[0], longitude: point[1] }]
      }
    }).reduce((pv, cv) => [...pv, ...cv], []).map((e, i) => ({ index: i , ...e}))
  }
} 