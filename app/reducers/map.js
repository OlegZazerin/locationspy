import { CHANGE_MARKERS } from '../actions/mapActions';

const initialData = {
    region: null,
    markers: [],
    directions: []
};

export function map (state = initialData, action = {}) {

    switch (action.type) {
        case CHANGE_MARKERS:
            return {
                ...state,
                markers: action.value
            }
        default:
            return state;
    }
}

export default map;
