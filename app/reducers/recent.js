import {
    SET_RECENT_DATA,
    UPDATE_PATHS,
    UPDATE_SEARCHES
} from '../actions/recentActions';

const initialData = {
    paths: [],
    searches: []
};

export function recent (state = initialData, action = {}) {
    switch (action.type) {
      case UPDATE_PATHS:
          return {
              ...state,
              paths: action.value
          }
      case UPDATE_SEARCHES:
          return {
              ...state,
              searches: action.value
          }
      case SET_RECENT_DATA:
          return {
            ...action.data
          }
      default:
          return state;
    }
}

export default recent;
