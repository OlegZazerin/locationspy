import { combineReducers } from 'redux';

import access from './access';
import map from './map';
import location from './location';
import direction from './direction';
import recent from './recent';

const rootReducer = combineReducers({
    access,
    map,
    location,
    direction,
    recent
});

export default rootReducer;
