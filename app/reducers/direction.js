import {
    ACTIVE_DIRECTION,
    SET_ACTUAL_LOCATION,
    SET_ORIGIN,
    SET_DESTINATION,
    SET_PATH,
    ADD_DIRECTIONS,
    LOADING_PATHS_START,
    LOADING_PATHS_SUCCESS,
    LOADING_PATHS_ERROR,
    CHANGE_AUTO_DEACTIVATION,
    SET_DIRECTION,
    SWAP_ORIGIN_AND_DESTINATION,

    SET_DESCRIPTION_CURRENT_STEP,
    SET_CURRENT_STEP,
    SET_NEXT_STEP,
    SET_COORDINATE_MOCK_LOCATION,
    SET_DESCRIPTION_MOCK_LOCATION,
    ADD_TO_PASSED_DISTANCE,
    ADD_PASSED_STEP,
    RESET_MOCK_DIRECTION_DATA,
    SET_STREET_NAME
} from '../actions/directionActions';

const initialData = {
    actualLocation: null,
    origin: null,
    destination: null,
    directions: [],//[{type: 'driving', data: test2}, {type: 'walking', data: test1}],
    direction: null,//{type: 'driving', data: test2},
    mockLocation: {
        index: null,
        coordinate: null,
        description: null
    },
    currentStep: null,
    streetName: '',
    nextStep: null,
    distanceTraveled: 0,
    passedSteps: null,
    timeLeft: null,
    distance: null,
    autoDeactivation: false,
    isActiveMockLocation: false
};

export function direction (state = initialData, action = {}) {
    console.log(action);
    switch (action.type) {
        case SET_ACTUAL_LOCATION:
              return {
                ...state,
                actualLocation: action.value
              }
        case SET_ORIGIN:
              return {
                ...state,
                origin: action.value,
                directions: [],
                direction: null
              }
        case SET_DESTINATION:
              return {
                ...state,
                destination: action.value,
                directions: [],
                direction: null
              }
        case SET_PATH:
              return {
                ...state,
                origin: action.origin,
                destination: action.destination,
                directions: [],
                direction: null
              }
        case SWAP_ORIGIN_AND_DESTINATION:
              return {
                ...state,
                origin: state.destination,
                destination: state.origin,
                directions: [],
                direction: null
              }
        case ADD_DIRECTIONS:
            return {
              ...state,
              directions: action.value
            }
        case LOADING_PATHS_START:
            return {
                ...state,
                directions: []
            }    
        // case LOADING_PATHS_SUCCESS:
        //     return {
        //       ...state,
        //       direction: state.directions.length ? state.directions[0]: null
        //     }
        case SET_DIRECTION:
            return {
              ...state,
              direction: action.value,
              timeLeft: action.value.data.duration.value
            }
        case CHANGE_AUTO_DEACTIVATION:
            return {
              ...state,
              autoDeactivation: action.value
            }
        case ACTIVE_DIRECTION:
          return {
            ...state,
            isActiveMockLocation: action.value
          }
        case SET_CURRENT_STEP:
          return {
            ...state,
            currentStep: action.value
          }
        case SET_DESCRIPTION_CURRENT_STEP:
          return {
            ...state,
            currentStep: {
              ...state.currentStep,
              description: action.value 
            }
          }  
        case SET_NEXT_STEP:
          return {
            ...state,
            nextStep: action.value
          }
        case SET_COORDINATE_MOCK_LOCATION:
          return {
            ...state,
            mockLocation: {
              ...state.mockLocation,
              index: action.value.index,
              coordinate: {
                  latitude: action.value.latitude,
                  longitude: action.value.longitude
                }
            },
            timeLeft: state.timeLeft - 1 
          }
        case SET_STREET_NAME:
          return {
            ...state,
            streetName: action.value
          }
        case ADD_TO_PASSED_DISTANCE:
          return {
            ...state,
            distance: state.distance + action.value
          }
         case ADD_PASSED_STEP:
          return {
            ...state,
            passedSteps: state.passedSteps ? [...state.passedSteps, action.value] : [action.value]
          }
        case RESET_MOCK_DIRECTION_DATA:
          return {
            ...state,
            passedSteps: initialData.passedSteps,
            distance: initialData.distance,
            time: initialData.time,
            mockLocation: initialData.mockLocation
          }                
        default:
            return state;
    }
}

export default direction;
