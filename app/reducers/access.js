const initialData = {
    isAllowedUseApp: undefined,
};

export function access (state = initialData, action = {}) {
    switch (action.type) {
        case 'CHANGE_FLAG_ALLOWED_USE_APP':
            return {
                ...state,
                isAllowedUseApp: action.value
            }    
        default:
            return state;
    }
}

export default access;
