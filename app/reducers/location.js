import {
    SET_ORIGINAL_LOCATION,
    CHANGE_MOCK_COORDINATE,
    CHANGE_LOCATION_DESCRIPTION,
    CHANGE_STATUS_MOCK_LOCATION
} from '../actions/appActions';

const initialData = {
    originalLocation: null,
    mockLocation: {
        placeId: null,
        coordinate: null,
        description: null
    },
    isActiveMockLocation: false
};

export function location (state = initialData, action = {}) {
    switch (action.type) {
        case SET_ORIGINAL_LOCATION:
            return {
                ...state,
                originalLocation: action.value
            }
        case CHANGE_MOCK_COORDINATE:
            return {
                ...state,
                mockLocation: {
                    placeId: null,
                    coordinate: action.value,
                    description: null
                }
            }
        case CHANGE_LOCATION_DESCRIPTION:
            return {
                ...state,
                mockLocation: {
                    ...state.mockLocation,
                    placeId: action.value.placeId,
                    description: action.value.description
                }
            }
        case CHANGE_STATUS_MOCK_LOCATION:
            return {
                ...state,
                isActiveMockLocation: action.value
            }
        default:
            return state;
    }
}

export default location;
