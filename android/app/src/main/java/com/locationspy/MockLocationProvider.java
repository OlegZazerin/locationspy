package com.locationspy;

import android.app.Activity;
import android.content.Context;
import android.provider.Settings.Secure;
import android.location.Location;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.location.LocationListener;
import android.os.Bundle;
import android.provider.Settings;
import android.content.pm.ApplicationInfo;

import android.app.AppOpsManager;
import android.os.Build;
import android.os.SystemClock;
import android.util.Log;
import java.util.List;
import android.widget.Toast;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.WritableMap;


public class MockLocationProvider extends ReactContextBaseJavaModule {

    //The React Native Context
    ReactApplicationContext mReactContext;

    private Location mLastLocation;
    private LocationListener mLocationListener;
    private LocationManager _locationManager;
    private Activity _activity;

    String _providerName;
    Context ctx;

    @Override
    public String getName() {
        return "MockLocation";
    }

    public MockLocationProvider(ReactApplicationContext reactContext) {
        super(reactContext);

        this.ctx = reactContext;
    }

    private boolean addProvider(String name){

        _providerName = name;
        _locationManager = (LocationManager) ctx.getSystemService(Context.LOCATION_SERVICE);

        if ((ctx.getApplicationInfo().flags & ApplicationInfo.FLAG_DEBUGGABLE) != 0) {

            _locationManager.addTestProvider(name, false, false, false, false, false, true, true, 0, 5);
            _locationManager.setTestProviderEnabled(name, true);

            return true;
        } else {
            Toast.makeText(ctx, "You need to enable developer mode", Toast.LENGTH_LONG).show();
        }

        return false;
    }

    private boolean checkFlagDebuggable(){
        boolean state = false;

        try {
            state = Settings.Secure.getInt(ctx.getContentResolver(), Settings.Global.DEVELOPMENT_SETTINGS_ENABLED) != 0;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return state;
    }

    private boolean checkAllowMockLocations(){
        boolean state = false;

        try {
            //if marshmallow
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                AppOpsManager opsManager = (AppOpsManager) ctx.getSystemService(Context.APP_OPS_SERVICE);
                state = (opsManager.checkOp(AppOpsManager.OPSTR_MOCK_LOCATION, android.os.Process.myUid(), BuildConfig.APPLICATION_ID)== AppOpsManager.MODE_ALLOWED);
            } else {
                // in marshmallow this will always return true
                state = !android.provider.Settings.Secure.getString(ctx.getContentResolver(), "mock_location").equals("0");
            }
        } catch (Exception e) {
            return state;
        }

        return state;
    }

//    public boolean isAllowMockLocations(Activity activity){
//        String t = Secure.getString(activity.getContentResolver(), Secure.ALLOW_MOCK_LOCATION);
//
//        if (t.equals("0")){
//            return false;
//        }
//        else{
//            return true;
//        }
//
//    }

    @ReactMethod
    public void getAllRequiredSettings(Callback callback) {
        boolean isDebuggable = checkFlagDebuggable();
        boolean isAllowedMockLocations = checkAllowMockLocations();

        callback.invoke(isDebuggable, isAllowedMockLocations);
    }

    @ReactMethod
    public void addGPSProvider(Promise promise){
        boolean r = this.addProvider(LocationManager.GPS_PROVIDER);

        if(r) {
            promise.resolve(Arguments.createMap());
        } else {
            promise.reject("Error", "Error");
        }
    }

    @ReactMethod
    public void addNetworkProvider(){ this.addProvider(LocationManager.NETWORK_PROVIDER); }

    @ReactMethod
    public void getLastKnownLocation(Promise promise){
        LocationManager lm = (LocationManager) ctx.getSystemService(Context.LOCATION_SERVICE);
        Location myLoc = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);

        if(myLoc == null) {
            myLoc = lm.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        }

        if(myLoc == null) {
            myLoc = lm.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
        }

        if(myLoc != null) {
            WritableMap map = Arguments.createMap();

            map.putDouble("latitude", myLoc.getLatitude());
            map.putDouble("longitude", myLoc.getLongitude());

            promise.resolve(map);
        } else {
            promise.reject("Error", "Error");
        }
    }

//    @ReactMethod
//    public void pushLocation(Double lat, Double lon, Promise promise) {
//
//        LocationManager lm = (LocationManager) ctx.getSystemService(Context.LOCATION_SERVICE);
//
//        List<String> list = lm.getAllProviders();
//
//        StringBuilder sb = new StringBuilder();
//
//        for (String s : list)
//        {
//            sb.append(s);
//            sb.append(", \t");
//        }
//
//        try{
//            Location mockLocation = new Location(LocationManager.GPS_PROVIDER);
//            mockLocation.setLatitude(lat);
//            mockLocation.setLongitude(lon);
//            mockLocation.setAccuracy(0);
//            mockLocation.setAltitude(0);
//            mockLocation.setBearing(0);
//            mockLocation.setSpeed(0);
//
//            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.JELLY_BEAN) {
//                mockLocation.setTime(SystemClock.elapsedRealtimeNanos());
//                mockLocation.setElapsedRealtimeNanos(SystemClock.elapsedRealtimeNanos());
//            } else {
//                mockLocation.setTime(System.currentTimeMillis());
//            }
//
//            lm.setTestProviderLocation(LocationManager.GPS_PROVIDER, mockLocation);
//
//            promise.resolve(Arguments.createMap());
//
//        } catch (Exception error){
//            promise.reject("Error", "Error");
//            Log.d("pushLocation: ", error.getMessage());
//        }
//    }

    @ReactMethod
    public void addNewLocation(Double lat, Double lon, Promise promise) {

//        _locationManager = (LocationManager) ctx.getSystemService(Context.LOCATION_SERVICE);

        try{
            Location location = new Location(LocationManager.GPS_PROVIDER);
            location.setLatitude(lat);
            location.setLongitude(lon);
            location.setAccuracy(0);
            location.setAltitude(0);
            location.setBearing(0);
            location.setSpeed(0);

            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.JELLY_BEAN) {
                location.setTime(SystemClock.elapsedRealtimeNanos());
                location.setElapsedRealtimeNanos(SystemClock.elapsedRealtimeNanos());
            } else {
                location.setTime(System.currentTimeMillis());
            }

            _locationManager.setTestProviderEnabled(_providerName, true);
            _locationManager.setTestProviderStatus(_providerName, LocationProvider.AVAILABLE, null, System.currentTimeMillis());
            _locationManager.setTestProviderLocation(_providerName, location);

            promise.resolve(Arguments.createMap());

        } catch (Exception error){
            promise.reject("Error", "Error");
            Log.d("pushLocation: ", error.getMessage());
        }
    }

    @ReactMethod
    public void disable(){
        LocationManager lm = (LocationManager) ctx.getSystemService(Context.LOCATION_SERVICE);
//        lm.removeTestProvider(LocationManager.GPS_PROVIDER);
        lm.setTestProviderEnabled(LocationManager.GPS_PROVIDER, false);
    }

    @ReactMethod
    public void clearLocation(){
        if(_providerName != null) {
            _locationManager.clearTestProviderLocation(_providerName);
        }
    }
}
